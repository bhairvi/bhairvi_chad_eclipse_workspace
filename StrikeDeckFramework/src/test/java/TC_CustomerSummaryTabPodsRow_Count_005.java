package StrikeDeckFramework.src.test.java;

import com.selenium.Pages.CustomerDetailsPage;
import com.selenium.Pages.CustomerSummaryPage;
import com.selenium.Pages.StrikeDeckHomePage;
import com.selenium.Pages.StrikeDeckLoginPage;
import com.selenium.commonLibs.BaseTest;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.JavascriptExecutor;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class TC_CustomerSummaryTabPodsRow_Count_005 extends BaseTest{
    @Test
    public void  CustomerSummaryTabPodsRow_Count() throws Exception {
        System.out.println("Customer Summary Tab Pods Row Count Test begins...");
        StrikeDeckLoginPage sdlp = new com.selenium.Pages.StrikeDeckLoginPage(driver);
        sdlp.loginToSD(username,password);
        String winHandleBefore = driver.getWindowHandle();
        Thread.sleep(3000);

        Set<String> winProps = driver.getWindowHandles();
        List<String> childWindowProps = new ArrayList(winProps);
        driver.switchTo().window(childWindowProps.get(1));
        driver.navigate().to("https://spear-uat.strikedeck.com/");

        StrikeDeckHomePage homePage = new StrikeDeckHomePage(driver);
        homePage.clickCustomer360TasksTab();
        waitforElementVisible(1000);
        homePage.clickspearOnlineTab();
        waitforElementVisible(1000);
        homePage.getcustomerSearchTitle();
        Assert.assertTrue(driver.getTitle().contains("Strikedeck |")  );

        //Actual test begins
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,350)", "");
        CustomerDetailsPage summaryTab = new CustomerDetailsPage(driver);
        summaryTab.clicksummaryTab();
        CustomerSummaryPage summaryPage = new CustomerSummaryPage(driver);
        String rowCountSeminarVocherText = summaryPage.getrowCountSeminarVocherText();
        System.out.println("Seminar Vouchers Total Records:" + rowCountSeminarVocherText);
        Assert.assertNotNull(rowCountSeminarVocherText);
        String rowCountCustomerContractsText = summaryPage.getrowCountCustomerContractsText();
        System.out.println("Customer Contracts Total Records: " + rowCountCustomerContractsText);
        Assert.assertNotNull(rowCountCustomerContractsText);
        driver.close();
        driver.quit();
        
    }
}
