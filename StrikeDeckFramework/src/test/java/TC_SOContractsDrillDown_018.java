package StrikeDeckFramework.src.test.java;
import StrikeDeckFramework.src.main.java.com.selenium.Pages.*;
import org.junit.Assert;
import org.junit.Test;
import com.selenium.commonLibs.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
public class TC_SOContractsDrillDown_018 extends BaseTest {
    @Test
    public void SOContractsDrillDown() throws Exception {
        System.out.println("Spear Online Contracts DrillDown Validation Test begins...");
        com.selenium.Pages.StrikeDeckLoginPage sdlp = new com.selenium.Pages.StrikeDeckLoginPage(driver);
        sdlp.loginToSD(username, password);
        String winHandleBefore = driver.getWindowHandle();
        Thread.sleep(3000);

        Set<String> winProps = driver.getWindowHandles();
        List<String> childWindowProps = new ArrayList(winProps);
        driver.switchTo().window(childWindowProps.get(1));
        driver.navigate().to("https://spear-uat.strikedeck.com/");

        //Actual test begins
        System.out.println("Click on the Spear Online Active Contracts Tile");
        SOContractsTilePage SOContacts = new SOContractsTilePage(driver);
        SOContacts.clickSOActiveContractsPodHeaderText();

        waitforElementVisible(5000);
        String activeContractsDrilldownPodHeaderText = SOContacts.getactiveContractsDrilldownHeaderText();
        waitforElementVisible(3000);
        String expectedText = "Drill-down: Active Contracts";
        Assert.assertEquals(expectedText, SOContacts.getactiveContractsDrilldownHeaderText());
        System.out.println("Pod Header is :" + activeContractsDrilldownPodHeaderText);

        String rowCountActiveContractsText = SOContacts.getrowCountactiveContracts();
        System.out.println("Drill-down: Active Contracts : " + rowCountActiveContractsText);
        Assert.assertNotNull(rowCountActiveContractsText);
        waitforElementVisible(3000);
        //Explicit Wait set up
        WebDriverWait elmentwait = new WebDriverWait(driver, 30);
        //Hovering over "Type of Order dropdown"
        Actions action = new Actions(driver);
        Thread.sleep(2000);
        WebElement dropdown = driver.findElement(By.xpath("//*[@id=\"segmentResultTable\"]//th[2]/div[2]/div/span/div/button"));
        action.moveToElement(dropdown).build().perform();

        //Clicking on "Type of Order dropdown"
        Thread.sleep(2000);
        action.moveToElement(dropdown).click().perform();
        System.out.println("Applying Type of Order Filter as New ");

        //Selecting the checkbox containing text as New ""
        Thread.sleep(2000);
        SOContacts.clicktypeofOrderNew();
        waitforElementVisible(3000);

        //Printing the total row count of  New ""
        String rowCountContact1 = SOContacts.getrowCountactiveContracts();
        System.out.println("Spear Online Type of Order New Active Contracts:" + rowCountContact1);
        waitforElementVisible(5000);
        Assert.assertNotNull(rowCountContact1);

        waitforElementVisible(1000);
        SOContacts.clickcustomer360();
        waitforElementVisible(3000);
        System.out.println("Renew Next 90days Contracts DrillDown Validation Test begins...");
        SOContacts.clickrenewNext90daysContractsPodHeaderText();

        waitforElementVisible(5000);
        String renewNext90daysContractsPodHeaderText = SOContacts.getrenewNext90daysContractsDrilldownHeaderText();
        waitforElementVisible(3000);
        String expectedText1 = "Drill-down: Active Contracts";
        Assert.assertEquals(expectedText1, SOContacts.getrenewNext90daysContractsDrilldownHeaderText());
        System.out.println("Pod Header is :" + renewNext90daysContractsPodHeaderText);

        String rowCountNext90daysContractsText = SOContacts.getrowCountrenewNext90daysContracts();
        System.out.println("Drill-down: Next 90daysContracts: " + rowCountNext90daysContractsText);
        Assert.assertNotNull(rowCountNext90daysContractsText);
        waitforElementVisible(3000);

        //Spear Online Renew Next 90days Contracts validation
        //Hovering over "Type Or Order dropdown"
        WebElement dropdown1 = driver.findElement(By.xpath("//*[@id=\"segmentResultTable\"]//th[2]/div[2]/div/span/div/button"));
        action.moveToElement(dropdown1).build().perform();
        //Clicking on "Type of Order dropdown"
        Thread.sleep(2000);
        action.moveToElement(dropdown1).click().perform();
        System.out.println("Applying Department Filter as Poach In ");
        //Selecting the checkbox containing text as Poach In ""
        Thread.sleep(2000);
        SOContacts.clicktypeofOrderPoachIn();
        waitforElementVisible(3000);
        //Printing the total row count of  Poach In ""
        String rowCountContact2 = SOContacts.getrowCountrenewNext90daysContracts();
        System.out.println("Spear Online Renew Next 90days Contracts:" + rowCountContact2);
        waitforElementVisible(5000);
        Assert.assertNotNull(rowCountContact2);


      //Spear Online Renew Last 60days Contracts validation
        SOContacts.clickcustomer360();
        waitforElementVisible(3000);
        System.out.println("Renew Last 60days Contracts DrillDown Validation Test begins...");
        SOContacts.clickrenewLast60daysContractsPodHeaderText();
        waitforElementVisible(5000);
        String renewLast60daysContractsPodHeaderText = SOContacts.getrenewLast60daysontractsDrilldownHeaderText();
        waitforElementVisible(3000);
        String expectedText2 = "Drill-down: Active Contracts";
        Assert.assertEquals(expectedText2, SOContacts.getrenewLast60daysontractsDrilldownHeaderText());
        System.out.println("Pod Header is :" + renewLast60daysContractsPodHeaderText);

        String rowCountLast60daysContractsText = SOContacts.getrowCountrenewNext90daysContracts();
        System.out.println("Drill-down: Last 60daysContracts: " + rowCountLast60daysContractsText);
        Assert.assertNotNull(rowCountLast60daysContractsText);
        waitforElementVisible(3000);

        //Hovering over "Type of Order dropdown"
        WebElement dropdown2 = driver.findElement(By.xpath("//*[@id=\"segmentResultTable\"]//th[2]/div[2]/div/span/div/button"));
        action.moveToElement(dropdown2).build().perform();
        //Clicking on "Type of Order dropdown"
        Thread.sleep(2000);
        action.moveToElement(dropdown2).click().perform();
        System.out.println("Applying Type of Orde Filter as Renewal");
        //Selecting the checkbox containing text as Renewal ""
        Thread.sleep(2000);
        SOContacts.clicktypeofOrderRenewal();
        waitforElementVisible(3000);
        //Printing the total row count of  Renewal ""
        String rowCountContact3 = SOContacts.getrowCountrenewLast60daysContracts();
        System.out.println("Spear Online Renew Next 90days Contracts:" + rowCountContact3);
        waitforElementVisible(5000);
        Assert.assertNotNull(rowCountContact3);
        driver.quit();

    }

}
