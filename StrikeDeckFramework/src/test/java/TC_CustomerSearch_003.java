package StrikeDeckFramework.src.test.java;

import com.selenium.Pages.StrikeDeckHomePage;
import com.selenium.Pages.StrikeDeckLoginPage;
import com.selenium.commonLibs.BaseTest;
import org.junit.Assert;
import org.junit.Test;

public class TC_CustomerSearch_003 extends BaseTest {

    @Test
    public void CustomerSearch() throws Exception {
        System.out.println("Customer Search Test begins...");

        StrikeDeckLoginPage sdlp = new StrikeDeckLoginPage(driver);
        sdlp.loginToSD(username,password);
        Thread.sleep(3000);
        driver.navigate().to("https://spear-uat.strikedeck.com/");
        String winHandleBefore = driver.getWindowHandle();
        driver.switchTo().window(winHandleBefore);
        Thread.sleep(2000);

        StrikeDeckHomePage homePage = new StrikeDeckHomePage(driver);
        homePage.clickCustomer360TasksTab();
            waitforElementVisible(2000);
        homePage.clickspearOnlineTab();
            waitforElementVisible(2000);
            homePage.getcustomerSearchTitle();
            Assert.assertTrue(driver.getTitle().contains("Strikedeck |")  );
            driver.quit();

    }


}
