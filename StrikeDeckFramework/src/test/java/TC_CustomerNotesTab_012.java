package StrikeDeckFramework.src.test.java;

import StrikeDeckFramework.src.main.java.com.selenium.Pages.CustomerNotesPage;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class TC_CustomerNotesTab_012 extends com.selenium.commonLibs.BaseTest {
    @Test
    public void CustomerNotesTab() throws Exception {
        System.out.println("Customer Notes Tabs Validation Test begins...");
        com.selenium.Pages.StrikeDeckLoginPage sdlp = new com.selenium.Pages.StrikeDeckLoginPage(driver);
        sdlp.loginToSD(username,password);
        String winHandleBefore = driver.getWindowHandle();
        Thread.sleep(3000);

        Set<String> winProps = driver.getWindowHandles();
        List<String> childWindowProps = new ArrayList(winProps);
        driver.switchTo().window(childWindowProps.get(1));
        driver.navigate().to("https://spear-uat.strikedeck.com/");

        com.selenium.Pages.StrikeDeckHomePage homePage = new com.selenium.Pages.StrikeDeckHomePage(driver);
        homePage.clickCustomer360TasksTab();
        waitforElementVisible(1000);
        homePage.clickspearOnlineTab();
        waitforElementVisible(1000);
        homePage.getcustomerSearchTitle();
        Assert.assertTrue(driver.getTitle().contains("Strikedeck |")  );

        //Actual test begins
        com.selenium.Pages.CustomerDetailsPage cdPage = new com.selenium.Pages.CustomerDetailsPage(driver);
        cdPage.clicknotesTab();
        waitforElementVisible(3000);
        System.out.println("Clicked on Notes Tab");

        //Creating New Notes FreeForm
        String notesXpath= "//div[@role='listitem']//div[text()='%s']";
        waitforElementVisible(3000);
        CustomerNotesPage NotesPage = new CustomerNotesPage(driver);
        NotesPage.clickNewNoteButton();
        waitforElementVisible(2000);
        NotesPage.clickFreeForm();
        System.out.println("Forms Notes clicked and Opened");
        waitforElementVisible(3000);
        //String freeforntextentered = "Auto Test From Notes";
        String freeforntextentered = "Auto Test FreeForm Notes " + RandomStringUtils.randomAlphabetic(5) ;
        driver.findElement(By.xpath("//div[22]/div/div[1]/div[2]/div[2]/div/div[2]")).sendKeys(freeforntextentered);
        waitforElementVisible(3000);
        driver.findElement(By.xpath("//div[22]/div/div[2]/button[2]")).click();
        System.out.println("Form Notes Has been created");
        waitforElementVisible(3000);
        //String freeformPublishValue = driver.findElement(By.xpath(String.format(notesXpath,freeforntextentered))).getText();
        //System.out.println(freeformPublishValue);
        Assert.assertEquals(freeforntextentered,driver.findElement(By.xpath(String.format(notesXpath,freeforntextentered))).getText());

       //Creating Meeting Notes FreeForm
        waitforElementVisible(3000);
        NotesPage.clickNewNoteButton();
        waitforElementVisible(2000);
        NotesPage.clickMeetingNote();
        System.out.println("Meeting Notes clicked and Opened");
        waitforElementVisible(3000);
        String meetingNotestextentered = "Auto Test MeetingNote"+ RandomStringUtils.randomAlphabetic(5) ;;
        driver.findElement(By.xpath("//div[2]/div[2]/div/div[2]")).sendKeys(meetingNotestextentered);
        waitforElementVisible(2000);
        driver.findElement(By.xpath("//body/div[22]/div/div[2]/button[2]")).click();
        System.out.println("Meeting Notes  Has been created");

        waitforElementVisible(3000);


        NotesPage.clickNewNoteButton();
        waitforElementVisible(2000);
        NotesPage.clickProfileNote();
        System.out.println("Profile Notes clicked and Opened");
        waitforElementVisible(3000);
        String profileNotestextentered = "Auto Test Profile Notes"+ RandomStringUtils.randomAlphabetic(5) ;
        driver.findElement(By.xpath("//div[2]/div[2]/div/div[2]")).sendKeys(profileNotestextentered);
        waitforElementVisible(2000);
        driver.findElement(By.xpath("//body/div[22]/div/div[2]/button[2]")).click();
        System.out.println("Profile Notes  Has been created");
        waitforElementVisible(3000);

        NotesPage.clickNewNoteButton();
        waitforElementVisible(2000);
        NotesPage.clickQBRNote();
        System.out.println("QBR Notes clicked and Opened");
        waitforElementVisible(3000);
        String qbrNotestextentered = "Auto Test QBR Notes"+ RandomStringUtils.randomAlphabetic(5) ;
        driver.findElement(By.xpath("//div[2]/div[2]/div/div[2]")).sendKeys(qbrNotestextentered);
        waitforElementVisible(2000);
        driver.findElement(By.xpath("//body/div[22]/div/div[2]/button[2]")).click();
        System.out.println("QBR Notes  Has been created");
        waitforElementVisible(3000);
    }
}