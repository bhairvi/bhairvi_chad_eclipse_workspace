package StrikeDeckFramework.src.test.java;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({TC_SDLogin_001.class,TC_HomePageValidation_002.class,TC_CustomerSearch_003.class})
public class RunnerTest {
}
