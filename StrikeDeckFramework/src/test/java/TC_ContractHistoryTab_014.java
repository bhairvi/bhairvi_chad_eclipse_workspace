package StrikeDeckFramework.src.test.java;
import StrikeDeckFramework.src.main.java.com.selenium.Pages.CustomerActivitiesPage;
import StrikeDeckFramework.src.main.java.com.selenium.Pages.CustomerContactsPage;
import StrikeDeckFramework.src.main.java.com.selenium.Pages.CustomerContractHistoryPage;
import org.junit.Assert;
import org.junit.Test;
import com.selenium.Pages.StrikeDeckLoginPage;
import com.selenium.Pages.StrikeDeckHomePage;
import com.selenium.Pages.CustomerDetailsPage;
import com.selenium.Pages.CustomerSummaryPage;
import com.selenium.commonLibs.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class TC_ContractHistoryTab_014 extends BaseTest {
    @Test
    public void ContractHistoryTab() throws Exception {
        System.out.println("Customer Contract History Tab Validation Test begins...");
        StrikeDeckLoginPage sdlp = new StrikeDeckLoginPage(driver);
        sdlp.loginToSD(username, password);
        String winHandleBefore = driver.getWindowHandle();
        Thread.sleep(3000);

        Set<String> winProps = driver.getWindowHandles();
        List<String> childWindowProps = new ArrayList(winProps);
        driver.switchTo().window(childWindowProps.get(1));
        driver.navigate().to("https://spear-uat.strikedeck.com/");

        StrikeDeckHomePage homePage = new StrikeDeckHomePage(driver);
        homePage.clickCustomer360TasksTab();
        waitforElementVisible(1000);
        homePage.clickspearOnlineTab();
        waitforElementVisible(1000);
        homePage.getcustomerSearchTitle();
        Assert.assertTrue(driver.getTitle().contains("Strikedeck |"));
        //Actual test begins
        CustomerDetailsPage cdPage = new CustomerDetailsPage(driver);
        cdPage.clickcontactHistoryTab();
        System.out.println("Click on Contract History Tab");

        //Getting Customer Contract History Pod Header Text

        CustomerContractHistoryPage contractHistoryPage = new CustomerContractHistoryPage(driver);
        waitforElementVisible(3000);
        String contractHistoryPodHeaderText = contractHistoryPage.getcontractHistoryPodHeaderText();
        waitforElementVisible(3000);
        String expectedTitle = "Contract History";
        Assert.assertEquals(expectedTitle,contractHistoryPage.getcontractHistoryPodHeaderText());
        System.out.println("Pod Header is :" + contractHistoryPodHeaderText);

        waitforElementVisible(3000);
        String rowCountContractHistoryPod = contractHistoryPage.getrowCountContractHistoryPodText();
        waitforElementVisible(3000);
        System.out.println("Row count is :" +rowCountContractHistoryPod);
        Assert.assertNotNull(rowCountContractHistoryPod);

        doPageScroll();

        System.out.println("Customer Contracts History Pod Department Filter selection Select All Test begins...");
        System.out.println("Applying Department Filter ");
        //Explicit Wait set up
        WebDriverWait elmentwait = new WebDriverWait(driver, 30);
        //Hovering over "Department dropdown"
        Actions action = new Actions(driver);
        Thread.sleep(2000);
        WebElement dropdown = driver.findElement(By.xpath("//*[@id='ContractBasedTable_5e18d40f3b24d36e75da3923']//th[5]/div[2]/div/span/div/button"));
        action.moveToElement(dropdown).build().perform();
        //Clicking on "Department dropdown"
        Thread.sleep(2000);
        action.moveToElement(dropdown).click().perform();
        //Selecting the checkbox containing text as "Select All"
        Thread.sleep(2000);
        CustomerContractHistoryPage selectAll = new CustomerContractHistoryPage(driver);
        selectAll.clickselectAllCheckBox();
        waitforElementVisible(5000);
        String rowCountContract = contractHistoryPage.getrowCountContractHistoryPodText();
        System.out.println("Contacts Pod Total Records:" + rowCountContract);
        waitforElementVisible(5000);
        Assert.assertNotNull(rowCountContract);
        selectAll.clickselectAllCheckBox();
        waitforElementVisible(5000);
        selectAll.clickcDoCSResidentCheckBox();
        waitforElementVisible(5000);
        System.out.println("Contacts Pod Total Records:" + rowCountContract);
        waitforElementVisible(5000);
        Assert.assertNotNull(rowCountContract);

        /*


        waitforElementVisible(3000);
        String customerContractPodHeaderText = contractHistoryPage.getcustomerContractPodHeaderText();
        waitforElementVisible(3000);
        String expectedTitle1 = "Customer Contract";
        Assert.assertEquals(expectedTitle,contractHistoryPage.getcustomerContractPodHeaderText());
        System.out.println("Pod Header is :" +customerContractPodHeaderText);


        waitforElementVisible(3000);
        String rowCountCustomerContractPodText = contractHistoryPage.getrowCountCustomerContractPodText();
        waitforElementVisible(3000);
        System.out.println("Row count is :"+rowCountCustomerContractPodText);
        Assert.assertNotNull(rowCountContractHistoryPod);
*/

    }
}