package StrikeDeckFramework.src.test.java;

import com.selenium.Pages.CustomerDetailsPage;
import com.selenium.Pages.CustomerSummaryPage;
import com.selenium.Pages.StrikeDeckHomePage;
import com.selenium.Pages.StrikeDeckLoginPage;
import com.selenium.commonLibs.BaseTest;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertTrue;

public class TC_CustomerSummaryTabExternalID_006 extends BaseTest {

    @Test
    public void CustomerSummaryTabExternalID() throws Exception {
        System.out.println("Customer Summary Tab ExternalID to Netsuite Test begins...");
        StrikeDeckLoginPage sdlp = new StrikeDeckLoginPage(driver);
        sdlp.loginToSD(username,password);
        String winHandleBefore = driver.getWindowHandle();
        Thread.sleep(3000);

        Set<String> winProps = driver.getWindowHandles();
        List<String> childWindowProps = new ArrayList(winProps);
        driver.switchTo().window(childWindowProps.get(1));
        driver.navigate().to("https://spear-uat.strikedeck.com/");
        //Opening New window for Netsuite.

        driver.switchTo().window(winHandleBefore);
        System.out.println(childWindowProps.size());
        sdlp.clickNetsuiteURL();
        System.out.println("Netsuite Stage Button clicked");
        winProps = driver.getWindowHandles();
        childWindowProps = new ArrayList(winProps);
        System.out.println(childWindowProps.size());

        driver.switchTo().window(childWindowProps.get(1));
        Thread.sleep(5000);
        StrikeDeckHomePage homePage = new StrikeDeckHomePage(driver);
        homePage.clickCustomer360TasksTab();
        waitforElementVisible(5000);
        homePage.clickspearOnlineTab();
        waitforElementVisible(5000);

        homePage.getcustomerSearchTitle();
        Assert.assertTrue(driver.getTitle().contains("Strikedeck |"));

        //Actual test begins
        CustomerDetailsPage summaryTab = new CustomerDetailsPage(driver);
        waitforElementVisible(5000);
        summaryTab.clicksummaryTab();

        CustomerSummaryPage summaryPage = new CustomerSummaryPage(driver);
        waitforElementVisible(5000);
        String generalInformationHeaderText = summaryPage.getgeneralInformationHeaderText();
        System.out.println(generalInformationHeaderText);
        waitforElementVisible(5000);
        summaryPage.clickexternalIdLink();
        System.out.println("External ID Link Has been clicked");
        Thread.sleep(5000);
        //store parent window value in string
        String parentWindow = driver.getWindowHandle();
        //store the set of all windows



        Set<String> allwindows= driver.getWindowHandles();
        int totalwindows = allwindows.size();

        int counter=0;
        for (String childWindow : allwindows) {
            if(counter<totalwindows){
                driver.switchTo().window(childWindow);
                System.out.println(driver.getTitle());
                if(StringUtils.containsIgnoreCase(driver.getTitle(),"Customer - NetSuite (Spear Staging (SB3))")){
                    System.out.println("Matched");
                }
                if(driver.getTitle().equalsIgnoreCase("Customer - NetSuite (Spear Staging (SB3))")){
                    System.out.println("Matched");
                }
            }
            counter++;

        }





/*
        for (String childWindow : allwindows) {
            if(!childWindow.equals(parentWindow))
            {
               driver.switchTo().window(childWindow);
               //System.out.println(driver.getTitle());
                //Assert.assertEquals(true, driver.getTitle().contains("Customer - NetSuite (Spear Staging (SB3))"));
                //driver.switchTo().window(childWindow);
                System.out.println(driver.getTitle());
                Assert.assertEquals(true, driver.getTitle().contains("Spear Education - My Applications"));
                //Assert.assertEquals(true, driver.getTitle().contains("Home - NetSuite (Spear Staging (SB3)"));
                //Assert.assertEquals(true, driver.getTitle().contains("Customer - NetSuite (Spear Staging (SB3))"));
                driver.close();
            }
        }
*/
        driver.switchTo().window(parentWindow);
    }
}