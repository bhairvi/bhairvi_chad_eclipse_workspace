package StrikeDeckFramework.src.test.java;

import StrikeDeckFramework.src.main.java.com.selenium.Pages.AllActiveContractsTilePage;
import StrikeDeckFramework.src.main.java.com.selenium.Pages.CustomerActivitiesPage;
import StrikeDeckFramework.src.main.java.com.selenium.Pages.CustomerContactsPage;
import StrikeDeckFramework.src.main.java.com.selenium.Pages.CustomerContractHistoryPage;
import org.junit.Assert;
import org.junit.Test;
import com.selenium.Pages.StrikeDeckLoginPage;
import com.selenium.Pages.StrikeDeckHomePage;
import com.selenium.Pages.CustomerDetailsPage;
import com.selenium.Pages.CustomerSummaryPage;
import com.selenium.commonLibs.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class TC_AllActiveContractsDrillDown_017 extends BaseTest {
    @Test
    public void AllActiveContractsDrillDown() throws Exception {
        System.out.println("All Active Contracts DrillDown Validation Test begins...");
        com.selenium.Pages.StrikeDeckLoginPage sdlp = new com.selenium.Pages.StrikeDeckLoginPage(driver);
        sdlp.loginToSD(username, password);
        String winHandleBefore = driver.getWindowHandle();
        Thread.sleep(3000);

        Set<String> winProps = driver.getWindowHandles();
        List<String> childWindowProps = new ArrayList(winProps);
        driver.switchTo().window(childWindowProps.get(1));
        driver.navigate().to("https://spear-uat.strikedeck.com/");

        //Actual test begins
        System.out.println("Click on the All Active Contracts Tile");
        AllActiveContractsTilePage allActiveContacts = new AllActiveContractsTilePage(driver);
        allActiveContacts.clickallActiveContractsPodHeaderText();

        waitforElementVisible(5000);
        String activeContractsDrilldownPodHeaderText = allActiveContacts.getactiveContractsDrilldownHeaderText();
        waitforElementVisible(3000);
        String expectedText = "Drill-down: Active Contracts";
        Assert.assertEquals(expectedText, allActiveContacts.getactiveContractsDrilldownHeaderText());
        System.out.println("Pod Header is :" + activeContractsDrilldownPodHeaderText);

        String rowCountActiveContractsText = allActiveContacts.getrowCountactiveContracts();
        System.out.println("Drill-down: Active Contracts : " + rowCountActiveContractsText);
        Assert.assertNotNull(rowCountActiveContractsText);
        waitforElementVisible(3000);
        //Explicit Wait set up
        WebDriverWait elmentwait = new WebDriverWait(driver, 30);
        //Hovering over "Department dropdown"
        Actions action = new Actions(driver);
        Thread.sleep(2000);
        WebElement dropdown = driver.findElement(By.xpath("//*[@id=\"segmentResultTable\"]//th[5]/div[2]/div/span/div/button"));
        action.moveToElement(dropdown).build().perform();

        //Clicking on "Department dropdown"
        Thread.sleep(2000);
        action.moveToElement(dropdown).click().perform();
        System.out.println("Applying Department Filter as CDOCS Fellow ");

        //Selecting the checkbox containing text as CDOCS Fellow ""
        Thread.sleep(2000);
        allActiveContacts.clickdepartmentCDOCSFellow();
        waitforElementVisible(3000);

        //Printing the total row count of  CDOCS Fellow ""
        String rowCountContact1 = allActiveContacts.getrowCountactiveContracts();
        System.out.println("CDOCS Fellow Active Contracts:" + rowCountContact1);
        waitforElementVisible(5000);
        Assert.assertNotNull(rowCountContact1);

        waitforElementVisible(1000);
        allActiveContacts.clickcustomer360();
        waitforElementVisible(3000);
        System.out.println("Renew Next 90days Contracts DrillDown Validation Test begins...");
        allActiveContacts.clickrenewNext90daysContractsPodHeaderText();

        waitforElementVisible(5000);
        String renewNext90daysContractsPodHeaderText = allActiveContacts.getrenewNext90daysContractsDrilldownHeaderText();
        waitforElementVisible(3000);
        String expectedText1 = "Drill-down: Active Contracts";
        Assert.assertEquals(expectedText1, allActiveContacts.getrenewNext90daysContractsDrilldownHeaderText());
        System.out.println("Pod Header is :" + renewNext90daysContractsPodHeaderText);

        String rowCountNext90daysContractsText = allActiveContacts.getrowCountrenewNext90daysContracts();
        System.out.println("Drill-down: Next 90daysContracts: " + rowCountNext90daysContractsText);
        Assert.assertNotNull(rowCountNext90daysContractsText);
        waitforElementVisible(3000);


        //Hovering over "Department dropdown"
        WebElement dropdown1 = driver.findElement(By.xpath("//*[@id=\"segmentResultTable\"]//th[5]/div[2]/div/span/div/button"));
        action.moveToElement(dropdown1).build().perform();
        //Clicking on "Department dropdown"
        Thread.sleep(2000);
        action.moveToElement(dropdown1).click().perform();
        System.out.println("Applying Department Filter as CDOCS Mentor");
        //Selecting the checkbox containing text as CDOCS Mentor ""
        Thread.sleep(2000);
        allActiveContacts.clickdepartmentCDOCSMentor();
        waitforElementVisible(3000);
        //Printing the total row count of  CDOCS Fellow ""
        String rowCountContact2 = allActiveContacts.getrowCountrenewNext90daysContracts();
        System.out.println("CDOCS Mentor Renew Next 90days Contracts:" + rowCountContact2);
        waitforElementVisible(5000);
        Assert.assertNotNull(rowCountContact2);

        allActiveContacts.clickcustomer360();
        waitforElementVisible(3000);
        System.out.println("Renew Last 60days Contracts DrillDown Validation Test begins...");
        allActiveContacts.clickrenewLast60daysContractsPodHeaderText();
        waitforElementVisible(5000);
        String renewLast0daysContractsPodHeaderText = allActiveContacts.getrenewLast60daysontractsDrilldownHeaderText();
        waitforElementVisible(3000);
        String expectedText2 = "Drill-down: Active Contracts";
        Assert.assertEquals(expectedText2, allActiveContacts.getrenewLast60daysontractsDrilldownHeaderText());
        System.out.println("Pod Header is :" + renewLast0daysContractsPodHeaderText);

        String rowCountLast60daysContractsText = allActiveContacts.getrowCountrenewNext90daysContracts();
        System.out.println("Drill-down: Last 60daysContracts: " + rowCountLast60daysContractsText);
        Assert.assertNotNull(rowCountLast60daysContractsText);
        waitforElementVisible(3000);

        //Hovering over "Department dropdown"
        WebElement dropdown2 = driver.findElement(By.xpath("//*[@id=\"segmentResultTable\"]//th[5]/div[2]/div/span/div/button"));
        action.moveToElement(dropdown2).build().perform();
        //Clicking on "Department dropdown"
        Thread.sleep(2000);
        action.moveToElement(dropdown2).click().perform();
        System.out.println("Applying Department Filter as CDOCS Resident");
        //Selecting the checkbox containing text as CDOCS Resident ""
        Thread.sleep(2000);
        allActiveContacts.clickdepartmentCDOCSResident();
        waitforElementVisible(3000);
        //Printing the total row count of  CDOCS Fellow ""
        String rowCountContact3 = allActiveContacts.getrowCountrenewLast60daysContracts();
        System.out.println("CDOCS Resident Renew Next 90days Contracts:" + rowCountContact3);
        waitforElementVisible(5000);
        Assert.assertNotNull(rowCountContact3);

        driver.quit();

        /*//Since data for CREC is not available so not using once data available we can use this code
        System.out.println("Applying Department Filter as CREC ");
        //Selecting the checkbox containing text as "Completed"
        Thread.sleep(2000);
        allActiveContacts.clickdepartmentCEREC();
        waitforElementVisible(3000);

         //Printing the total row count of CREC "
        String rowCountContact1 = allActiveContacts.getrowCountactiveContracts();
        System.out.println("CEREC Active Contracts:" + rowCountContact1);
        waitforElementVisible(5000);
        Assert.assertNotNull(rowCountContact1);*/


    }

}

