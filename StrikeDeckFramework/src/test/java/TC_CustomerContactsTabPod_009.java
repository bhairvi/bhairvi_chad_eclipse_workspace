package StrikeDeckFramework.src.test.java;

import StrikeDeckFramework.src.main.java.com.selenium.Pages.CustomerContactsPage;
import com.selenium.Pages.CustomerDetailsPage;
import com.selenium.Pages.StrikeDeckHomePage;
import com.selenium.commonLibs.BaseTest;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class TC_CustomerContactsTabPod_009 extends BaseTest {
    @Test
    public void CustomerContactsTabPod() throws Exception {
        System.out.println("Customer Contacts Tab  Contacts Pod Validation Test begins...");
        com.selenium.Pages.StrikeDeckLoginPage sdlp = new com.selenium.Pages.StrikeDeckLoginPage(driver);
        sdlp.loginToSD(username, password);
        String winHandleBefore = driver.getWindowHandle();
        Thread.sleep(3000);

        Set<String> winProps = driver.getWindowHandles();
        List<String> childWindowProps = new ArrayList(winProps);
        driver.switchTo().window(childWindowProps.get(1));
        driver.navigate().to("https://spear-uat.strikedeck.com/");

        StrikeDeckHomePage homePage = new StrikeDeckHomePage(driver);
        homePage.clickCustomer360TasksTab();
        waitforElementVisible(1000);
        homePage.clickspearOnlineTab();
        waitforElementVisible(1000);
        homePage.getcustomerSearchTitle();
        Assert.assertTrue(driver.getTitle().contains("Strikedeck |"));

        CustomerDetailsPage cdPage = new CustomerDetailsPage(driver);
        cdPage.clickcontactsTab();
        System.out.println("Click on Contacts Tab");
        CustomerContactsPage contactsPod = new CustomerContactsPage(driver);
        waitforElementVisible(5000);
        String contactspodHeaderText = contactsPod.getcustomerContactsPodHeaderText();
        waitforElementVisible(5000);
        System.out.println("Pod Header is :" + contactspodHeaderText);

        //to perform Scroll on application using Selenium

        doPageScroll();

        //CustomerContactsPage dropdown = new CustomerContactsPage(driver);
        WebDriverWait wait = new WebDriverWait(driver, 30);
        System.out.println("Customer Contacts Tab  Contacts Pod Role Filter selection Select All Test begins...");
        System.out.println("Applying Role Filter ");
        //Explicit Wait set up
        WebDriverWait elmentwait = new WebDriverWait(driver, 30);
        //Hovering over "Role dropdown"
        Actions action = new Actions(driver);
        Thread.sleep(2000);
        WebElement dropdown = driver.findElement(By.xpath("//*[@id=\"contactsTable\"]//button"));
        action.moveToElement(dropdown).build().perform();
        //Clicking on "Role dropdown"
        Thread.sleep(2000);
        action.moveToElement(dropdown).click().perform();
        //Selecting the checkbox containing text as "Select All"
        Thread.sleep(2000);
        CustomerContactsPage selectAll = new CustomerContactsPage(driver);
        selectAll.clickselectAll();
        waitforElementVisible(5000);
        String rowCountContact = contactsPod.getrowCountCustomerContactsText();
        System.out.println("Contacts Pod Total Records:" + rowCountContact);
        waitforElementVisible(5000);
        Assert.assertNotNull(rowCountContact);
        //driver.quit();
        //Selecting Default Contacts as role filter
        System.out.println("Customer Contacts Tab  Contacts Pod Role Filter selection Default contact Test begins...");

        action.moveToElement(dropdown).build().perform();
        //Clicking on "Role dropdown"
        Thread.sleep(2000);
        action.moveToElement(dropdown).click().perform();
        //Selecting the checkbox containing text as "Default contact"
        Thread.sleep(2000);
        CustomerContactsPage selectdefaultContact = new CustomerContactsPage(driver);
        selectdefaultContact.clickdefaultContact();
        waitforElementVisible(5000);
        //Getting row count
        String defaultContactrowCountContact = contactsPod.getrowCountCustomerContactsText();
        System.out.println("Contacts Pod Total Records:" + defaultContactrowCountContact);
        waitforElementVisible(5000);
        Assert.assertNotNull(defaultContactrowCountContact);
        driver.quit();

       /* System.out.println("Customer Contacts Tab  Contacts Pod Role Filter selection Front Office Team Test begins...");
        Thread.sleep(2000);
        action.moveToElement(dropdown).build().perform();
        //Clicking on "Role dropdown"
        Thread.sleep(2000);
        action.moveToElement(dropdown).click().perform();
        WebDriverWait wait1 = new WebDriverWait(driver, 60);
        wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"contactsTable\"]//li[1]"))).isSelected();
        WebDriverWait wait2 = new WebDriverWait(driver, 60);
        wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"contactsTable\"]//li[1]"))).click();
        //Selecting the checkbox containing text as "Front Office Team "
        WebDriverWait wait3 = new WebDriverWait(driver, 60);
        wait3.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"contactsTable\"]//li[8]/a/label"))).click();
        String frontOfficeTeamContactrowCountContact = contactsPod.getrowCountCustomerContactsText();
        System.out.println("Contacts Pod Total Records:" + frontOfficeTeamContactrowCountContact);
        waitforElementVisible(5000);
        Assert.assertNotNull(frontOfficeTeamContactrowCountContact);*/


    }
}