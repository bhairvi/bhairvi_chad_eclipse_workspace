package StrikeDeckFramework.src.test.java;

import com.selenium.Pages.CustomerDetailsPage;
import com.selenium.Pages.CustomerSummaryPage;
import com.selenium.Pages.StrikeDeckHomePage;
import com.selenium.Pages.StrikeDeckLoginPage;
import com.selenium.commonLibs.BaseTest;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class TC_CustomerSummaryTab_004 extends BaseTest {
    @Test
    public void CustomerSummaryTab() throws Exception {
        System.out.println("Customer Summary Tab Test begins...");
        StrikeDeckLoginPage sdlp = new StrikeDeckLoginPage(driver);
        sdlp.loginToSD(username, password);
        String winHandleBefore = driver.getWindowHandle();
        Thread.sleep(5000);

        Set<String> winProps = driver.getWindowHandles();
        List<String> childWindowProps = new ArrayList(winProps);
        driver.switchTo().window(childWindowProps.get(1));
        driver.navigate().to("https://spear-uat.strikedeck.com/");
        //Opening New window for Netsuite.

        driver.switchTo().window(winHandleBefore);
        System.out.println(childWindowProps.size());
        sdlp.clickNetsuiteURL();
        System.out.println("Netsuite Stage Button clicked");
        winProps = driver.getWindowHandles();
        childWindowProps = new ArrayList(winProps);
        System.out.println(childWindowProps.size());
        // driver.switchTo().window(childWindowProps.get(2));
        driver.switchTo().window(childWindowProps.get(1));
        Thread.sleep(1000);
        //driver.switchTo().window(childWindowProps.get(2));

        StrikeDeckHomePage homePage = new StrikeDeckHomePage(driver);
        homePage.clickCustomer360TasksTab();
        waitforElementVisible(1000);
        homePage.clickspearOnlineTab();
        waitforElementVisible(1000);
        homePage.getcustomerSearchTitle();
        Assert.assertTrue(driver.getTitle().contains("Strikedeck |"));

        //Actual test begins
        CustomerDetailsPage summaryTab = new CustomerDetailsPage(driver);
        waitforElementVisible(5000);
        summaryTab.clicksummaryTab();
        CustomerSummaryPage summaryPage = new CustomerSummaryPage(driver);
        summaryPage.getHeaderTexts();


        waitforElementVisible(6000);
        //driver.switchTo().window(childWindowProps.get(3));
        summaryPage.clickexternalIdLink();
        System.out.println("NetSuite External ID Link Has been clicked");
        Thread.sleep(1000);
        driver.switchTo().window(childWindowProps.get(1));
        Thread.sleep(1000);
        summaryPage.clickwebsiteLink();
        System.out.println("Website Link Has been clicked");
        Thread.sleep(5000);

        Set<String> windows = driver.getWindowHandles();
        System.out.println(windows);
        System.out.println("Customer Details page ");
        for (String window : windows) {
            driver.switchTo().window(window);
            if (driver.getTitle().contains("Dentist - Woodbury, MN - Dr. Marko Kamel"))
                break;
            {
                System.out.println("Doctor Website Page ");
            }
        }
        driver.switchTo().window(childWindowProps.get(1));
        Thread.sleep(1000);
        summaryPage.clickemailLink();
        System.out.println("Outlook email ID Link Has been clicked");

        //driver.quit();
    }
}
