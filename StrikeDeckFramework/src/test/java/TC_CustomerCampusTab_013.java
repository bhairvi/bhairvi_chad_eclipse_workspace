package StrikeDeckFramework.src.test.java;

import StrikeDeckFramework.src.main.java.com.selenium.Pages.CustomerActivitiesPage;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class TC_CustomerCampusTab_013 extends com.selenium.commonLibs.BaseTest {
    @Test
    public void CustomerCampusTab() throws Exception {
        System.out.println("Customer Notes Tabs Validation Test begins...");
        com.selenium.Pages.StrikeDeckLoginPage sdlp = new com.selenium.Pages.StrikeDeckLoginPage(driver);
        sdlp.loginToSD(username, password);
        String winHandleBefore = driver.getWindowHandle();
        Thread.sleep(3000);

        Set<String> winProps = driver.getWindowHandles();
        List<String> childWindowProps = new ArrayList(winProps);
        driver.switchTo().window(childWindowProps.get(1));
        driver.navigate().to("https://spear-uat.strikedeck.com/");

        com.selenium.Pages.StrikeDeckHomePage homePage = new com.selenium.Pages.StrikeDeckHomePage(driver);
        homePage.clickCustomer360TasksTab();
        waitforElementVisible(1000);
        homePage.clickspearOnlineTab();
        waitforElementVisible(1000);
        homePage.getcustomerSearchTitle();
        Assert.assertTrue(driver.getTitle().contains("Strikedeck |"));
        //Actual test begins
        com.selenium.Pages.CustomerDetailsPage cdPage = new com.selenium.Pages.CustomerDetailsPage(driver);
        cdPage.clickCampusTab();
        System.out.println("Click on Campus Tab");

        //Getting Customer Phone Calls Pod Header Text
        waitforElementVisible(3000);
        CustomerActivitiesPage activitiesPage = new CustomerActivitiesPage(driver);
        String customerphonecallsPodHeaderText = activitiesPage.getcustomerPhoneCallsPodHeaderText();
        waitforElementVisible(3000);
        System.out.println("Pod Header is :" + customerphonecallsPodHeaderText);

    }
}