package com.selenium.commonLibs;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;


public class BaseTest {

    public static WebDriver driver;
    //public String baseUrl="https://spear-uat.strikedeck.com/";
    public String username="qa_ns_automation@speareducation.com";
    public String password="20PAssword20";
    //public static Logger logger;
    @BeforeClass
    public static void setup()
    {
        System.setProperty("webdriver.chrome.driver",
                "StrikeDeckFramework/src/test/resources/chromedriver.exe");
        driver=new ChromeDriver();
        driver.get("https://speareducation.okta.com/");
        System.out.println("Okta URL is opened");
        driver.manage().deleteAllCookies();
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        //Logger logger=Logger.getLogger("StrikeDeck");
        //PropertyConfigurator.configure("log4j.properties");
    }
    @AfterClass
    public static void tearDown()
    {
        //driver.quit();
    }
   /* @parameters{"browserType","OS","version","environment","exec       Envirponment"}
    public void configure(String browsertYpe,String os,String ver,String env,String execEnv){

        if(env.equalsIgnoreCase("QA"))
        if(browsertYpe.equalsIgnoreCase("Chrome"))
        {

        }
    }*/
    public void waitforElementVisible(long milliseconds){
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
