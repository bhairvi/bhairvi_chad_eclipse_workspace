package com.selenium.commonLibs;

import org.openqa.selenium.WebElement;

import com.selenium.contract.ICheckbox;

public class CheckBoxConrol implements ICheckbox{

	@Override
	public void changeCheckBoxStatus(WebElement element, boolean status) throws Exception {
		
		if( element.isSelected() && !status ) {
		element.click();
		} else if (!element.isSelected() && status) {
			element.click();
		}
		}

}