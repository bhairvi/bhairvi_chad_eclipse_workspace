package com.selenium.commonLibs;


import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import com.selenium.contract.IJavaScripts;

public class javascriptControl implements IJavaScripts{

	private WebDriver driver;
	
	public javascriptControl (WebDriver driver) {
		this.driver= driver;
	}

	private JavascriptExecutor getJsEngine()throws Exception
	{
		JavascriptExecutor jsEngine =(JavascriptExecutor) driver;
		return jsEngine;
		
	}

	
	@Override
	public void executeJavaScript(String scriptToExecute) throws Exception 
	{
		getJsEngine().executeScript(scriptToExecute);
			}


	@Override
	public void scrollDown(int x, int y) throws Exception {
	String jsCommand = String.format("window.scrollBy(%d),%d",x,y);
		getJsEngine().executeScript(jsCommand);	
	}

	@Override
	public String executeJavaScriptWithReturnValue(String scriptToExecute) throws Exception {
		String returnvalue = getJsEngine().executeScript(scriptToExecute).toString();
		return returnvalue;
	}

}
