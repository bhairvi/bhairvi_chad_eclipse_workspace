package com.selenium.TestData;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class ConfigFileReader {
    private Properties properties;
    private final String propertyFilePath = "configs//configuration.properties";


    public ConfigFileReader() {
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(propertyFilePath));
            properties = new Properties();
            try {
                properties.load(reader);
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Configuration.properties not Present at " + propertyFilePath);
        }
    }

    public String getChromeDriverPath() {
        String driverPath = properties.getProperty("chromeDriverPath");
        if (driverPath != null) {
            return driverPath;
        } else throw new RuntimeException("chromeDriverPath not specified in the Configuration.properties file.");
    }
    public String getStrikeDeckUrl() {
        String url = properties.getProperty("strikeDeckURL");
        if (url != null) {
            return url;
        } else throw new RuntimeException("url not specified in the Configuration.properties file.");
    }


    public String getStikeDeckUser() {
        String user = properties.getProperty("strikeDeckUser");
        if (user != null) {
            return user;
        } else throw new RuntimeException("password not specified in the Configuration.properties file.");
    }

    public String getStrikeDeckePassword() {
        String password = properties.getProperty("strikeDeckPassword");
        if (password != null) {
            return password;
        } else throw new RuntimeException("password not specified in the Configuration.properties file.");
    }

    public enum Browser {
        CHROME("chrome");

        private String browser;

        Browser(String browser) {
            this.browser = browser;
        }

        public String get() {
            return browser;
        }

        //****** Reverse Lookup Implementation************//

        //Lookup table
        private static final Map<String, Browser> lookup = new HashMap<>();

        //Populate the lookup table on loading time
        static {
            for (Browser env : Browser.values()) {
                lookup.put(env.get(), env);
            }
        }

        //This method can be used for reverse lookup purpose
        public static Browser get(String browser) {
            return lookup.get(browser);
        }
    }

    public Browser getBrowser() {
        String browserString = properties.getProperty("browser");
        Browser browser = Browser.get(browserString);
        if (browser == null) {
            throw new RuntimeException("browser not specified in the Configuration.properties file.");
        }

        return browser;
    }
    public enum ExecutionLocation {
        REMOTE, LOCAL
    }

    public ExecutionLocation getExecutionLocation() {
        String executionLocation = properties.getProperty("executionLocation");
        switch (executionLocation) {
            case "local":
                return ExecutionLocation.LOCAL;

            case "remote":
                return ExecutionLocation.REMOTE;

            default:
                throw new RuntimeException("browser not specified in the Configuration.properties file.");
        }
    }
    public String getStrikeDeckEnvironment() {
        String  strikeDeckEnvironment = properties.getProperty("strikeDeckEnvironment");
        if (strikeDeckEnvironment != null) {
            return strikeDeckEnvironment;
        } else throw new RuntimeException("StrikeDeckEnvironment not specified in the Configuration.properties file.");
    }
    public String getOktaUrl() {
        String url = properties.getProperty("oktaURL");
        if (url != null) {
            return url;
        } else throw new RuntimeException("url not specified in the Configuration.properties file.");
    }

    public String getOktaUser() {
        String user = properties.getProperty("oktaUser");
        if (user != null) {
            return user;
        } else throw new RuntimeException("Okta User not specified in the Configuration.properties file.");
    }

    public String getOktaPassword() {
        String password = properties.getProperty("oktaPassword");
        if (password != null) {
            return password;
        } else throw new RuntimeException("Okta Password not specified in the Configuration.properties file.");
    }

    public String getLoginMethod() {
        String loginMethod = properties.getProperty("loginMethod");
        if (loginMethod != null) {
            return loginMethod;
        } else throw new RuntimeException("Login Method not specified in the Configuration.properties file.");

    }

}

