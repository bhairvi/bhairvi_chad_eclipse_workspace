package StrikeDeckFramework.src.main.java.com.selenium.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.selenium.commonLibs.WebDriverUtils;

public class AllActiveContractsTilePage {


    WebDriver ldriver;

    public AllActiveContractsTilePage(WebDriver rdriver) {
        ldriver = rdriver;
        PageFactory.initElements(rdriver, this);
    }

    WebDriverUtils webDriverUtils = new WebDriverUtils();

    @FindBy(xpath = "//*[@id=\"tileHelperWithBuilder_4bb53fad-68bf-4454-92a5-b607db95b4e1\"]//h2")
    WebElement allActiveContractsPodHeaderText;
    @FindBy(xpath = "/html/body/div[1]/section/div/div/div/div[1]/h4")
    WebElement activeContractsDrilldownHeaderText;
    @FindBy(className = "pagination-info")
    WebElement rowCountactiveContracts;
    @FindBy(className = "multiselect dropdown-toggle btn btn-default")
    WebElement departmentDropdown;
    ;
    @FindBy(xpath = "//*[@id=\"segmentResultTable\"]//th[5]/div[2]/div/span/div/ul/li[2]/a/label")
    WebElement departmentCEREC;
    @FindBy(xpath = "//*[@id=\"segmentResultTable\"]//th[5]/div[2]/div/span/div/ul/li[3]/a/label")
    WebElement departmentCDOCSFellow;
    @FindBy(xpath = "//*[@id=\"segmentResultTable\"]//th[5]/div[2]//li[4]/a/label")
    WebElement departmentCDOCSMentor;
    @FindBy(xpath = "//*[@id=\"segmentResultTable\"]//div[2]/div/span/div/ul/li[5]/a/label")
    WebElement departmentCDOCSResident;


    @FindBy(id = "sidebar_customer360")
    WebElement customer360;

    @FindBy(xpath = "//*[@id=\"tileHelperWithBuilder_4bb53fad-68bf-4454-92a5-b607db95b4e1\"]//ul/li[1]/h5")
    WebElement renewNext90daysContractsPodHeaderText;
    @FindBy(className = "pagination-info")
    WebElement rowCountrenewNext90daysContracts;
    @FindBy(xpath = "/html/body/div[1]/section/div/div/div/div[1]/h4")
    WebElement renewNext90daysContractsDrilldownHeaderText;

    @FindBy(xpath = "//*[@id=\"tileHelperWithBuilder_4bb53fad-68bf-4454-92a5-b607db95b4e1\"]//ul/li[2]/h5")
    WebElement renewLast60daysContractsPodHeaderText;
    @FindBy(className = "pagination-info")
    WebElement rowCountrenewLast60daysContracts;
    @FindBy(xpath = "/html/body/div[1]/section/div/div/div/div[1]/h4")
    WebElement renewLast60daysontractsDrilldownHeaderText;



    public void clickallActiveContractsPodHeaderText() throws Exception {
        webDriverUtils.click(allActiveContractsPodHeaderText);
    }

    public String getactiveContractsDrilldownHeaderText() throws Exception {
        return webDriverUtils.getText(activeContractsDrilldownHeaderText);
    }

    public String getrowCountactiveContracts() throws Exception {
        return webDriverUtils.getText(rowCountactiveContracts);
    }


    public void clickdepartmentDropdown() throws Exception {
        webDriverUtils.click(departmentDropdown);
    }

    public void clickdepartmentCEREC() throws Exception {
        webDriverUtils.click(departmentCEREC);
    }

    public void clickdepartmentCDOCSFellow() throws Exception {
        webDriverUtils.click(departmentCDOCSFellow);
    }


    public void clickdepartmentCDOCSMentor() throws Exception {
        webDriverUtils.click(departmentCDOCSMentor);
    }

    public void clickdepartmentCDOCSResident() throws Exception {
        webDriverUtils.click(departmentCDOCSResident);
    }

    public void clickcustomer360() throws Exception {
        webDriverUtils.click(customer360);
    }

    public void clickrenewNext90daysContractsPodHeaderText() throws Exception {
        webDriverUtils.click(renewNext90daysContractsPodHeaderText);
    }

    public String getrowCountrenewNext90daysContracts() throws Exception {
        return webDriverUtils.getText(rowCountrenewNext90daysContracts);
    }

    public String getrenewNext90daysContractsDrilldownHeaderText() throws Exception {
        return webDriverUtils.getText(renewNext90daysContractsDrilldownHeaderText);

    }
    public void clickrenewLast60daysContractsPodHeaderText() throws Exception {
        webDriverUtils.click(renewLast60daysContractsPodHeaderText);
    }
    public String getrowCountrenewLast60daysContracts() throws Exception {
        return webDriverUtils.getText(rowCountrenewLast60daysContracts);
    }

    public String getrenewLast60daysontractsDrilldownHeaderText() throws Exception {
        return webDriverUtils.getText(renewLast60daysontractsDrilldownHeaderText);
    }
}
