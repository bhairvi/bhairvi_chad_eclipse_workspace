package StrikeDeckFramework.src.main.java.com.selenium.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CustomerTasksPage {

    WebDriver ldriver;

    public CustomerTasksPage(WebDriver rdriver) {
        ldriver = rdriver;
        PageFactory.initElements(rdriver, this);
    }

    com.selenium.commonLibs.WebDriverUtils webDriverUtils = new com.selenium.commonLibs.WebDriverUtils();

    @FindBy(xpath = "//*[@id=\"tabTitle\"]")
    WebElement customerTasksHeaderText;

    @FindBy(xpath = "//*[@id=\"taskListView\"]/nav/div/h6")
    WebElement customerTasksTotalcount;


    @FindBy(xpath = "//*[@id=\"pod_178_5ecedb9398b22c475abb3d0a\"]/div[2]/div[2]/div/span[2]/div/button")
    WebElement statusDropdownArrow;

    @FindBy(xpath = "//*[@id=\"pod_178_5ecedb9398b22c475abb3d0a\"]/div[2]/div[2]/div/span[2]/div/ul/li[3]/a/label")
    WebElement completed;

    @FindBy(xpath = "//*[@id=\"basic-addon2\"]/i")
    WebElement search;


    public String getcustomerTasksHeaderText() throws Exception {
        return webDriverUtils.getText(customerTasksHeaderText);
    }


    public String getcustomerTasksTotalcount() throws Exception {
        return webDriverUtils.getText(customerTasksTotalcount);
    }

    public void clickstatusDropdownArrow() throws Exception{
        webDriverUtils.click(statusDropdownArrow);
    }
    public void clickcompleted() throws Exception{
        webDriverUtils.click(completed);
    }
    public void clicksearch() throws Exception{
        webDriverUtils.click(search);
    }
}
