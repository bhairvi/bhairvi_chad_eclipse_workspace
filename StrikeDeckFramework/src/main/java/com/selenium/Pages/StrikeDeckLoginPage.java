package com.selenium.Pages;

import com.selenium.commonLibs.WebDriverUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class StrikeDeckLoginPage {

    WebDriver ldriver;
    WebDriverUtils webDriverUtils = new WebDriverUtils();

    public StrikeDeckLoginPage(WebDriver rdriver) {
        ldriver = rdriver;
        PageFactory.initElements(rdriver, this);
    }

    @FindBy(id = "okta-signin-username")
    @CacheLookup
    WebElement txtUsername;

    @FindBy(id = "okta-signin-password")
    @CacheLookup
    WebElement txtPassword;

    @FindBy(id = "okta-signin-submit")
    @CacheLookup
    WebElement btnLogin;
    @FindBy(xpath = "//a[contains(@href, 'https://speareducation.okta.com/home/bookmark/0oa5eez2phwDXCh4o357/2557?fromHome=true')]")
    WebElement oktaLogin;
    @FindBy(xpath = "//a[contains(@href, 'https://speareducation.okta.com/home/netsuite/0oa11p2k4r2ruSv5P357/82?fromHome=true')]")
    WebElement netSuiteLogin;

    public void setUserName(String userName) {
        webDriverUtils.enterText(txtUsername, userName);
    }

    public void setPassword(String password) {
        webDriverUtils.enterText(txtPassword, password);
    }

    public void clickSubmit() {
        webDriverUtils.click(btnLogin);
    }

    public void clickSDURL() {
        webDriverUtils.click(oktaLogin);
    }
    public void clickNetsuiteURL(){ webDriverUtils.click(netSuiteLogin);}

    public void loginToSD(String userName, String password) {
            //Fill user name
        this.setUserName(userName);
        System.out.println("Okta Test User email id entered");
            //Fill password
        this.setPassword(password);
        System.out.println("Okta Test User password entered");
            //Click Login button
        this.clickSubmit();
        System.out.println("Login Button clicked");
        this.clickSDURL();
        System.out.println("SD QA Button clicked");

    }


}

