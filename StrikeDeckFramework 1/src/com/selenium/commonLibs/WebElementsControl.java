package com.selenium.commonLibs;

import org.openqa.selenium.WebElement;

import com.selenium.contract.IWebElements;

public class WebElementsControl implements IWebElements
{

	@Override
	public void click(WebElement element) throws Exception {
		element.click();
	}

	@Override
	public String getText(WebElement element) throws Exception {
		String text = element.getText(); 
		return text;
	}

	@Override
	public String getAttribute(WebElement element, String Attribute) throws Exception {
		String Attributevalue= element.getAttribute(Attribute);
		return Attributevalue;
	}

	@Override
	public String getCssValue(WebElement element, String csspropertyName) throws Exception {
		String cssvalue= element.getCssValue(csspropertyName);
		return cssvalue;
	}

	@Override
	public boolean isElementEnabled(WebElement element) throws Exception {
		boolean status = element.isEnabled();
		return status;
	}

	@Override
	public boolean isElementVisble(WebElement element) throws Exception {
		boolean status =element.isDisplayed();
		return status;
	}

	@Override
	public boolean isElementSelected(WebElement element) throws Exception {
		boolean status =element.isSelected();
		return status;
	}

}
