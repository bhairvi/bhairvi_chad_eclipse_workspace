package com.selenium.commonLibs;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import com.selenium.contract.ICommonDriver;


public class CommonDriver implements ICommonDriver {

	private WebDriver driver;
	private long pageLoadTimeout;
	private long elementdetectionTimeout;
	
public CommonDriver(String browserType) throws Exception  {
	
	browserType = browserType.trim();
	
switch(browserType.toLowerCase()) {
  
/*case "chrome":

	System.setProperty("webdriver.chrome.driver", 
			"C:\\Users\\bchad\\eclipse-workspace\\Libs\\chromedriver.exe");
	
	driver = new ChromeDriver();
break;
	
case "Mozilla":

		System.setProperty("webdriver.gecko.driver", 
				"C:\\Users\\bchad\\eclipse-workspace\\Libs\\chromedriver.exe");
		
		driver = new FirefoxDriver();
break;*/
		
case "ie":

		System.setProperty("webdriver.ie.driver", 
				"C:\\Users\\bchad\\eclipse-workspace\\Libs\\chromedriver.exe");
		
		driver = new InternetExplorerDriver();
		
break;
		
		
default:
	throw new Exception("Invalid Browser Type: " + browserType);
			
}	

pageLoadTimeout =60;
elementdetectionTimeout= 60;

}
@Override
	public void InvokeBrowser(String url) throws Exception {
	
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(pageLoadTimeout, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(elementdetectionTimeout, TimeUnit.SECONDS);
		driver.get(url);
		
		
	}

@Override
	public void setPageLoadTimeOut(long pageLoadTimeout) throws Exception {
		 
		this.pageLoadTimeout = pageLoadTimeout;
	}

@Override
	public void setElementDetectionTimeOut(long elementdetectionTimeout) throws Exception {
		
		this.elementdetectionTimeout = elementdetectionTimeout;
	}

public WebDriver getDriver()throws Exception {
	return driver;
}
@Override	
	public String getTitle() throws Exception {
		// TODO Auto-generated method stub
		String TitleOfthePage= driver.getTitle();
		return TitleOfthePage;
	}

@Override	
	public void closeBrowser() throws Exception {
if(driver!= null) {
	driver.close();
}
	}

@Override	
	public void closeAllBrowser() throws Exception {
	if(driver!= null) {
	driver.quit();
	}
	}

@Override	
	public void navigatetoUrl() throws Exception {

	String url= "https://spear-stage2.strikedeck.com/login";
	driver.get(url);

	}

@Override	
	public void navigateback() throws Exception {
		// TODO Auto-generated method stub
	if(driver!=null) {	
	driver.navigate().back();
	}
	}

@Override	
	public void navigateforward() throws Exception {
		// TODO Auto-generated method stub
	if(driver!=null) {
	driver.navigate().forward();
	}
	}

@Override
	public void reloadpage() throws Exception {
		// TODO Auto-generated method stub
	if(driver!=null) {	
	driver.navigate().refresh();
	}
	}

}
