package com.selenium.contract;

import org.openqa.selenium.WebElement;

public interface IWebElements {

	public void click(WebElement element) throws Exception;
	
	public String getText(WebElement element) throws Exception;
	
	public String getAttribute(WebElement element, String Attribute) throws Exception;
	
	public String getCssValue(WebElement element, String csspropertyName) throws Exception;
	
	public boolean isElementEnabled(WebElement element) throws Exception;
	
	public boolean isElementVisble(WebElement element) throws Exception;
	
	public boolean isElementSelected(WebElement element) throws Exception;

}
