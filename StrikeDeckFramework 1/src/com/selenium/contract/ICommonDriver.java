package com.selenium.contract;

import org.openqa.selenium.WebDriver;

public interface ICommonDriver {

	public void InvokeBrowser(String url) throws Exception;
	
	public void setPageLoadTimeOut(long pageLoadTimeout) throws Exception;
	
	public void setElementDetectionTimeOut(long elementdetectionTimeout)throws Exception;
	
	public String getTitle() throws Exception;
	
	public void closeBrowser() throws Exception;
	
	public void closeAllBrowser() throws Exception;
	
	public void navigatetoUrl() throws Exception;
	
	public void navigateback() throws Exception;

	public void navigateforward() throws Exception;

	public void reloadpage() throws Exception;
	 
	public WebDriver getDriver() throws Exception;

	
}