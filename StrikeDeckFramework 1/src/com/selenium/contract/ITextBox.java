package com.selenium.contract;

import org.openqa.selenium.WebElement;

public interface ITextBox {
	
	public void setText(WebElement element, String textToSet ) throws Exception;
	
	public void clearText(WebElement element) throws Exception;


}
