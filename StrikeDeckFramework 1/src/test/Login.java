package test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.selenium.commonLibs.CommonDriver;
import com.selenium.commonLibs.DropDownControl;
import com.selenium.commonLibs.ScreenshotControl;
import com.selenium.commonLibs.TextBoxControl;
import com.selenium.commonLibs.WebElementsControl;

public class Login {

	public static void main(String[] args) {
		
		String url= "https://spear-uat.strikedeck.com/";
		
		try {
			CommonDriver cmndriver = new CommonDriver("ie");
			WebDriver driver = cmndriver.getDriver();
			cmndriver.setPageLoadTimeOut(40l);
			cmndriver.setElementDetectionTimeOut(60l);
			cmndriver.InvokeBrowser(url);
			TextBoxControl textbox= new TextBoxControl();		
			WebElementsControl elementcontrol = new WebElementsControl();
			DropDownControl dropdown = new DropDownControl();
			ScreenshotControl screenshot= new ScreenshotControl(driver);
			

			
			WebElement Oktausername = driver.findElement(By.id("okta-signin-username"));
			WebElement Oktapassword = driver.findElement(By.id("okta-signin-password"));
			WebElement Signin  = driver.findElement(By.id("okta-signin-submit"));
			textbox.setText(Oktausername, "qa_ns_automation@speareducation.com");
			textbox.setText(Oktapassword, "20PAssword20");
			elementcontrol.click(Signin);
			cmndriver.setPageLoadTimeOut(40l);
			WebElement repfilter = driver.findElement(By.id("showfilter_representative"));
			dropdown.selectViaVisbleText(repfilter, "Representative");
			//dropdown.selectByVisibleText("Representative");
			
			screenshot.captureandsavescreenshot("C:\\Users\\bchad\\eclipse-workspace\\StrikeDeckFramework\\output\\screenshot.jpeg");
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}

}
