package com.selenium.contract;

import java.util.List;

import org.openqa.selenium.WebElement;

public interface IDropdown {
	
	public void selectViaVisbleText(WebElement element, String visibleText) throws Exception;
	
	public void selectViaValue(WebElement element, String value) throws Exception;
	
	public void selectViaIndex(WebElement element, int Index) throws Exception;
	
	public boolean isMultiple(WebElement element) throws Exception;
	
	public List<WebElement> getAllOption(WebElement element) throws Exception;
	
	public List<WebElement> getAllSelectedOptions(WebElement element) throws Exception;
		
	public WebElement getFirstSelectedOption(WebElement element) throws Exception;

}
