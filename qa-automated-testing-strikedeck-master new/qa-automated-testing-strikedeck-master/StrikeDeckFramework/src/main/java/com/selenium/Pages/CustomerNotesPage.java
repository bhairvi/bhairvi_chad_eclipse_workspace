package StrikeDeckFramework.src.main.java.com.selenium.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CustomerNotesPage {
    WebDriver ldriver;

    public CustomerNotesPage(WebDriver rdriver) {
        ldriver = rdriver;
        PageFactory.initElements(rdriver, this);
    }

    com.selenium.commonLibs.WebDriverUtils webDriverUtils = new com.selenium.commonLibs.WebDriverUtils();

    @FindBy(xpath = "//*[@id=\"note-root\"]/div[2]//div[3]/div[1]")
    WebElement newNoteButton;
    @FindBy(xpath = "//*[@id=\"note-root\"]//div[1]/span")
    WebElement freeform;
    @FindBy(xpath = "//*[@id=\"note-root\"]//div[2]/span")
    WebElement meetingnote;
    @FindBy(xpath = "//*[@id=\"note-root\"]//div[3]/span")
    WebElement profilenote;
    @FindBy(xpath = "//*[@id=\"note-root\"]//div[4]/span")
    WebElement qbrnote;

    public void clickNewNoteButton() throws Exception{
        webDriverUtils.click(newNoteButton);
    }
    public void clickFreeForm() throws Exception{
        webDriverUtils.click(freeform);
    }
    public void clickMeetingNote() throws Exception{
        webDriverUtils.click(meetingnote);
    }
    public void clickProfileNote() throws Exception{
        webDriverUtils.click(profilenote);
    }
    public void clickQBRNote() throws Exception{
        webDriverUtils.click(qbrnote);
    }

}


