package com.selenium.commonLibs;


import com.selenium.contract.IWebElements;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class WebElementsControl implements IWebElements
{

	@Override
	public void click(WebElement element) throws Exception {
		element.click();
	}

	@Override
	public String getText(WebElement element) throws Exception {
		String text = element.getText(); 
		return text;
	}

	@Override
	public String getAttribute(WebElement element, String Attribute) throws Exception {
		String Attributevalue= element.getAttribute(Attribute);
		return Attributevalue;
	}

	@Override
	public String getCssValue(WebElement element, String csspropertyName) throws Exception {
		String cssvalue= element.getCssValue(csspropertyName);
		return cssvalue;
	}

	@Override
	public boolean isElementEnabled(WebElement element) throws Exception {
		boolean status = element.isEnabled();
		return status;
	}

	@Override
	public boolean isElementVisble(WebElement element) throws Exception {
		boolean status =element.isDisplayed();
		return status;
	}

	@Override
	public boolean isElementSelected(WebElement element) throws Exception {
		boolean status =element.isSelected();
		return status;
	}

	@Override
	public void enterText(WebElement element, String testData) throws Exception {
		element.sendKeys(testData);
	}

	@Override
	public void pressEnterKey(WebElement element ) {
		element.sendKeys(Keys.ENTER);
	}

}
