package StrikeDeckFramework.src.main.java.com.selenium.Pages;
import com.selenium.commonLibs.WebDriverUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CustomerActivitiesPage {

    WebDriver ldriver;

    public CustomerActivitiesPage(WebDriver rdriver) {
        ldriver = rdriver;
        PageFactory.initElements(rdriver, this);
    }

    WebDriverUtils webDriverUtils = new WebDriverUtils();

    @FindBy(xpath = "//h4[contains(text(),'Customer Phone Calls')]")
    WebElement customerPhoneCallsPodHeaderText;
    @FindBy(xpath = "//*[@id=\"5fda33b5a0c52213f7fbcc97\"]/td[8]/a")
    WebElement callRecordingLink;
    @FindBy(xpath = "//*[@id=\"pod_156_5eab5db77d3f5f27066b33ed\"]//div[3]/div[1]/span[1]")
    WebElement rowCountCustomerPhoneCallsPodText;
    @FindBy(xpath = "//*[@id=\"pod_31_5d9898c63b24d3111dabd113\"]//h1")
    WebElement emailActivitesHeaderText;
    @FindBy(xpath = "//h6[contains(text(),'Total : ')]")
            ////*[@id="emailActivitiesTable"]//div/h6
    WebElement emailActivitesTotalcount;
    @FindBy(xpath = "//h4[contains(text(),'NetSuite Tasks')]")
    WebElement netSuitesTasksPodHeaderText;
    @FindBy(xpath = "//*[@id=\"pod_124_5e21f47f3b24d3148cf03ac7\"]//div[3]/div[1]/span[1]")
    WebElement rowCountnetSuitesTasksText;
    @FindBy(xpath = "//*[@id='tabTitle']")
    WebElement customerTasksHeaderText;
    @FindBy(xpath = "//*[@id=\"taskListView\"]//div/h6")
    WebElement customerTasksTotalcount;
    @FindBy(xpath = "//*[@id=\"note-root\"]//div[3]/div[1]")
    WebElement newNotebutton;
    @FindBy(xpath = "//*[@id=\"note-root\"]//div[1]/span")
    WebElement freeform;
    @FindBy(xpath = "//*[@id=\"note-root\"]//div[2]/span")
    WebElement meetingnote;
    @FindBy(xpath = "//*[@id=\"note-root\"]//div[3]/span")
    WebElement profilenote;
    @FindBy(xpath = "//*[@id=\"note-root\"]//div[4]/span")
    WebElement qbrnote;


    public String getcustomerPhoneCallsPodHeaderText() throws Exception {
        return webDriverUtils.getText(customerPhoneCallsPodHeaderText);
    }
    public void clickCallRecordingLink() throws Exception{
        webDriverUtils.click(callRecordingLink);
    }

    public String getemailActivitesHeaderText() throws Exception {
        return webDriverUtils.getText(emailActivitesHeaderText);
    }

    public String getemailActivitesTotalcount() throws Exception {
        return webDriverUtils.getText(emailActivitesTotalcount);
    }

    public String getnetSuitesTasksPodHeaderText() throws Exception {
        return webDriverUtils.getText(netSuitesTasksPodHeaderText);
    }

    public String getcustomerTasksHeaderText() throws Exception {
        return webDriverUtils.getText(customerTasksHeaderText);
    }

    public String getcustomerTasksTotalcount() throws Exception {
        return webDriverUtils.getText(customerTasksTotalcount);
    }
      public String getrowCountCustomerPhoneCallsPodText() throws Exception
    {
        return webDriverUtils.getText(rowCountCustomerPhoneCallsPodText);

    }
    public String getrowCountnetSuitesTasksText() throws Exception
    {
        return webDriverUtils.getText(rowCountnetSuitesTasksText);

    }
       public void clickNewNoteButton() throws Exception{
        webDriverUtils.click(newNotebutton);
    }

    public void clickFreeForm() throws Exception{
        webDriverUtils.click(freeform);
    }
    public void clickMeetingNote() throws Exception{
        webDriverUtils.click(meetingnote);
    }
    public void clickProfileNote() throws Exception{
        webDriverUtils.click(profilenote);
    }
    public void clickQBRNote() throws Exception{
        webDriverUtils.click(qbrnote);
    }
}
