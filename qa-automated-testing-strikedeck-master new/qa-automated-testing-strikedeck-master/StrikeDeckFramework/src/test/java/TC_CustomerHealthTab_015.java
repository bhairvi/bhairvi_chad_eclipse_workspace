package StrikeDeckFramework.src.test.java;

import StrikeDeckFramework.src.main.java.com.selenium.Pages.CustomerActivitiesPage;
import StrikeDeckFramework.src.main.java.com.selenium.Pages.CustomerDetailsPage;
import StrikeDeckFramework.src.main.java.com.selenium.Pages.CustomerHealthPage;
import com.selenium.Pages.StrikeDeckHomePage;
import StrikeDeckFramework.src.main.java.com.selenium.Pages.StrikeDeckLoginPage;
import org.junit.Assert;
import org.junit.Test;

public class TC_CustomerHealthTab_015 extends com.selenium.commonLibs.BaseTest {

    @Test
    public void CustomerHealthTab() throws Exception {
        System.out.println("Customer Health Test begins...");

        StrikeDeckLoginPage sdlp = new StrikeDeckLoginPage(driver);
        sdlp.loginToSD(username,password);
        Thread.sleep(3000);
        driver.navigate().to("https://spear-uat.strikedeck.com/");
        String winHandleBefore = driver.getWindowHandle();
        driver.switchTo().window(winHandleBefore);
        Thread.sleep(2000);

        StrikeDeckHomePage homePage = new StrikeDeckHomePage(driver);
        homePage.clickCustomer360TasksTab();
        waitforElementVisible(2000);
        homePage.clickspearOnlineTab();
        waitforElementVisible(2000);
        homePage.getSearchexistingcustomer();
        Assert.assertTrue(driver.getTitle().contains("Strikedeck |")  );

        CustomerDetailsPage cdPage = new CustomerDetailsPage(driver);
        cdPage.clickcustomerHealthTab();
        System.out.println("Click on Customer Health Tab");

        //Getting Customer Health Metric Drill Down Pod Header Text
        waitforElementVisible(2000);
        CustomerHealthPage customerHealthPage = new CustomerHealthPage(driver);
        waitforElementVisible(2000);
        String customerHealthMetricDrillDown = customerHealthPage.getcustomerHealthMetricDrillDownPodHeaderText();
        waitforElementVisible(2000);
        String expectedTitle = "Customer Health Metric Drill Down";
        Assert.assertEquals(expectedTitle,customerHealthPage.getcustomerHealthMetricDrillDownPodHeaderText());
        System.out.println("Pod Header is :" + customerHealthMetricDrillDown);

        String textWarningText = customerHealthPage.gettextWarningText();
        waitforElementVisible(3000);
        String expectedTitle1 = "Loggedin user don't have permission to view Usage";
        Assert.assertEquals(expectedTitle1,customerHealthPage.gettextWarningText());
        System.out.println("Warning Text :" + textWarningText);

        //driver.quit();

    }


}
