package StrikeDeckFramework.src.test.java;

import StrikeDeckFramework.src.main.java.com.selenium.Pages.CustomerActivitiesPage;
import StrikeDeckFramework.src.main.java.com.selenium.Pages.CustomerDetailsPage;
import StrikeDeckFramework.src.main.java.com.selenium.Pages.StrikeDeckLoginPage;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class TC_CustomerActivitiesTab_010 extends com.selenium.commonLibs.BaseTest {
    @Test
    public void CustomerActivitiesTab() throws Exception {
        System.out.println("Customer Activities Tab deatails Validation Test begins...");
        StrikeDeckLoginPage sdlp = new StrikeDeckLoginPage(driver);
        sdlp.loginToSD(username,password);
        String winHandleBefore = driver.getWindowHandle();
        Thread.sleep(3000);

        Set<String> winProps = driver.getWindowHandles();
        List<String> childWindowProps = new ArrayList(winProps);
        driver.switchTo().window(childWindowProps.get(1));
        driver.navigate().to("https://spear-uat.strikedeck.com/");

        com.selenium.Pages.StrikeDeckHomePage homePage = new com.selenium.Pages.StrikeDeckHomePage(driver);
        homePage.clickCustomer360TasksTab();
        waitforElementVisible(1000);
        homePage.clickspearOnlineTab();
        waitforElementVisible(1000);
        homePage.getcustomerSearchTitle();
        Assert.assertTrue(driver.getTitle().contains("Strikedeck |")  );

        //Actual test begins
        CustomerDetailsPage cdPage = new CustomerDetailsPage(driver);
        cdPage.clickactivitiesTab();
        System.out.println("Click on Contacts Tab");

        //Getting Customer Phone Calls Pod Header Text
        CustomerActivitiesPage activitiesPage = new CustomerActivitiesPage(driver);
        String customerphonecallsPodHeaderText = activitiesPage.getcustomerPhoneCallsPodHeaderText();
        waitforElementVisible(3000);
        System.out.println("Pod Header is :" +customerphonecallsPodHeaderText );

        //Click on call recording link

        activitiesPage.clickCallRecordingLink();
        System.out.println("Call Recording Hyperlink been clicked");

        String parentWindow = driver.getWindowHandle();

        //store the set of all windows
        Set<String> allwindows= driver.getWindowHandles();
        int totalwindows = allwindows.size();

        String expectedUrl = "https://na1.nice-incontact.com/login/#/";
        int counter=0;
        for (String childWindow : allwindows) {
            if(counter<totalwindows){
                driver.switchTo().window(childWindow);
                System.out.println(driver.getCurrentUrl());
                if(driver.getCurrentUrl().equalsIgnoreCase("https://na1.nice-incontact.com/login/#/")){
                    System.out.println("Matched");
                }
            }
            counter++;

        }
        driver.switchTo().window(parentWindow);

       doPageScroll();

        //Getting Customer Phone Calls Pod row count
        waitforElementVisible(3000);
        String rowCountCustomerPhoneCallsPodText = activitiesPage.getrowCountCustomerPhoneCallsPodText();
        System.out.println("Customer Phone Calls Total Records:" + rowCountCustomerPhoneCallsPodText);
        Assert.assertNotNull(rowCountCustomerPhoneCallsPodText);

        //Getting Customer Email Activities Header Text

        String emailActivitesHeaderTex = activitiesPage.getemailActivitesHeaderText();
        waitforElementVisible(3000);
        System.out.println("Email Activites Header is :" +emailActivitesHeaderTex );

        //Getting Customer Email Activities Total count
        waitforElementVisible(3000);
        String emailActivitesTotalcount = activitiesPage.getemailActivitesTotalcount();
        System.out.println("Customer Email Activites Total Records: " + emailActivitesTotalcount);
        Assert.assertNotNull(emailActivitesTotalcount);

        //Getting Customer NetSuite Tasks Header Text
        String netSuitesTasksPodHeaderText = activitiesPage.getnetSuitesTasksPodHeaderText();
        waitforElementVisible(3000);
        System.out.println("NetSuite Tasks Header is :" +netSuitesTasksPodHeaderText );

        //Getting Customer NetSuite Tasks row count
        waitforElementVisible(3000);
        String rowCountnetSuitesTasksText = activitiesPage.getrowCountnetSuitesTasksText();
        System.out.println("Customer NetSuite Tasks Total Records: " + rowCountnetSuitesTasksText);
        Assert.assertNotNull(rowCountnetSuitesTasksText);

        //Getting Customer Customer Tasks Header Text
        String customerTasksHeaderText = activitiesPage.getcustomerTasksHeaderText();
        waitforElementVisible(3000);
        System.out.println("Customer Tasks Header is :" +customerTasksHeaderText );

        //Getting Customer Customer Tasks row count
        waitforElementVisible(3000);
        String customerTasksTotalcount = activitiesPage.getcustomerTasksTotalcount();
        System.out.println("Customer Customer Tasks Total Records: " + customerTasksTotalcount);
        Assert.assertNotNull(customerTasksTotalcount);

        //Click on New Note and select Free Form
        activitiesPage.clickNewNoteButton();
        waitforElementVisible(3000);
        activitiesPage.clickFreeForm();
        driver.findElement(By.xpath("/html/body/div[31]/div/div[1]/div[2]/div[2]/div/div[2]")).sendKeys("Test1");
        waitforElementVisible(5000);
        driver.findElement(By.xpath("//div[31]/div/div[2]/button[2]")).click();


       // driver.quit();

}
}
