package StrikeDeckFramework.src.test.java;

import StrikeDeckFramework.src.main.java.com.selenium.Pages.CustomerHealthPage;
import StrikeDeckFramework.src.main.java.com.selenium.Pages.CustomerNPSPage;
import StrikeDeckFramework.src.main.java.com.selenium.Pages.StrikeDeckHomePage;
import StrikeDeckFramework.src.main.java.com.selenium.Pages.StrikeDeckLoginPage;
import StrikeDeckFramework.src.main.java.com.selenium.Pages.CustomerDetailsPage;
import com.selenium.commonLibs.BaseTest;
import org.junit.Assert;
import org.junit.Test;

public class TC_CustomerNPS_016 extends BaseTest {

    @Test
    public void CustomerNPSTab() throws Exception {
        System.out.println("Customer NPS Test begins...");

        StrikeDeckLoginPage sdlp = new StrikeDeckLoginPage(driver);
        sdlp.loginToSD(username,password);
        Thread.sleep(3000);
        driver.navigate().to("https://spear-uat.strikedeck.com/");
        String winHandleBefore = driver.getWindowHandle();
        driver.switchTo().window(winHandleBefore);
        Thread.sleep(2000);

        StrikeDeckHomePage homePage = new StrikeDeckHomePage(driver);
        homePage.clickCustomer360TasksTab();
        waitforElementVisible(2000);
        homePage.clickspearOnlineTab();
        waitforElementVisible(2000);
        homePage.getSearchexistingcustomer();
        Assert.assertTrue(driver.getTitle().contains("Strikedeck |")  );

       CustomerDetailsPage cdPage = new CustomerDetailsPage(driver);
        cdPage.clicknpsTab();
        System.out.println("Click on Customer NPS Tab");

        //GettingCustomer NPS Score Responses Pod Header Text
        waitforElementVisible(2000);
        CustomerNPSPage customerNPSPage = new CustomerNPSPage(driver);
        waitforElementVisible(2000);
        String customerNPSResponses = customerNPSPage.getcustomerNPSScoreResponsesPodHeaderText();
        waitforElementVisible(2000);
        String expectedTitle1 = "Customer NPS Score Responses";
        Assert.assertEquals(expectedTitle1,customerNPSPage.getcustomerNPSScoreResponsesPodHeaderText());
        System.out.println("Pod Header is :" + customerNPSResponses);

        String recordText1 = customerNPSPage.getrecordsText1();
        waitforElementVisible(3000);
        String expectedTitle = "No matching records found";
        Assert.assertEquals(expectedTitle,customerNPSPage.getrecordsText1());
        System.out.println("Records Text :" + recordText1);

        //GettingCustomer NPS Score Last 365 Days Pod Header Text
        waitforElementVisible(2000);
        String customerNPSResponses365days = customerNPSPage.getcustomerNPSScoreLast365daysPodHeaderText();
        waitforElementVisible(2000);
        String expectedTitle2 = "NPS Score (Promoters & Detractors) - Last 365 Days";
        Assert.assertEquals(expectedTitle2,customerNPSPage.getcustomerNPSScoreLast365daysPodHeaderText());
        System.out.println("Pod Header is :" + customerNPSResponses365days);

        String recordText2 = customerNPSPage.getrecordsText1();
        waitforElementVisible(3000);
        Assert.assertEquals(expectedTitle,customerNPSPage.getrecordsText1());
        System.out.println("Records Text :" + recordText2);

        //driver.quit();

    }


}
