package StrikeDeckFramework.src.test.java;

import StrikeDeckFramework.src.main.java.com.selenium.Pages.CustomerDetailsPage;
import StrikeDeckFramework.src.main.java.com.selenium.Pages.CustomerSummaryPage;
import StrikeDeckFramework.src.main.java.com.selenium.Pages.StrikeDeckHomePage;
import StrikeDeckFramework.src.main.java.com.selenium.Pages.StrikeDeckLoginPage;
import com.selenium.commonLibs.BaseTest;
import org.junit.Assert;
import org.junit.Test;

public class TC_CustomerSearch_003 extends BaseTest {

    @Test
    public void CustomerSearch() throws Exception {
        System.out.println("Customer Search Test begins...");

        StrikeDeckLoginPage sdlp = new StrikeDeckLoginPage(driver);
        sdlp.loginToSD(username,password);
        Thread.sleep(3000);
        driver.navigate().to("https://spear-uat.strikedeck.com/");
        String winHandleBefore = driver.getWindowHandle();
        driver.switchTo().window(winHandleBefore);
        Thread.sleep(2000);

        StrikeDeckHomePage homePage = new StrikeDeckHomePage(driver);
        homePage.clickCustomer360TasksTab();
            waitforElementVisible(2000);
        homePage.clickspearOnlineTab();
            waitforElementVisible(2000);
            homePage.getcustomerSearchTitle();
            Assert.assertTrue(driver.getTitle().contains("Strikedeck |")  );
            driver.quit();

    }


}
