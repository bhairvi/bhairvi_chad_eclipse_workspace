package StrikeDeckFramework.src.test.java;

import StrikeDeckFramework.src.main.java.com.selenium.Pages.CustomerDetailsPage;
import StrikeDeckFramework.src.main.java.com.selenium.Pages.CustomerSummaryPage;
import com.selenium.Pages.StrikeDeckHomePage;
import StrikeDeckFramework.src.main.java.com.selenium.Pages.StrikeDeckLoginPage;
import com.selenium.commonLibs.BaseTest;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.JavascriptExecutor;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class TC_CustomerSummaryTabPodsRow_Count_005 extends BaseTest{
    @Test
    public void  CustomerSummaryTabPodsRow_Count() throws Exception {
        System.out.println("Customer Summary Tab Pods Row Count Test begins...");
        StrikeDeckLoginPage sdlp = new StrikeDeckLoginPage(driver);
        sdlp.loginToSD(username,password);
        String winHandleBefore = driver.getWindowHandle();
        Thread.sleep(3000);

        Set<String> winProps = driver.getWindowHandles();
        List<String> childWindowProps = new ArrayList(winProps);
        driver.switchTo().window(childWindowProps.get(1));
        driver.navigate().to("https://spear-uat.strikedeck.com/");

        StrikeDeckHomePage homePage = new StrikeDeckHomePage(driver);
        homePage.getSearchexistingcustomer();
        Assert.assertTrue(driver.getTitle().contains("Strikedeck |"));

        //Actual test begins
        doPageScroll();

        CustomerDetailsPage summaryTab = new CustomerDetailsPage(driver);
        summaryTab.clicksummaryTab();
        CustomerSummaryPage summaryPage = new CustomerSummaryPage(driver);
        String rowCountSeminarVocherText = summaryPage.getrowCountSeminarVocherText();
        System.out.println("Seminar Vouchers Total Records:" + rowCountSeminarVocherText);
        Assert.assertNotNull(rowCountSeminarVocherText);
        String rowCountCustomerContractsText = summaryPage.getrowCountCustomerContractsText();
        System.out.println("Customer Contracts Total Records: " + rowCountCustomerContractsText);
        Assert.assertNotNull(rowCountCustomerContractsText);
        String rowCountActiveContractsText = summaryPage.getrowCountActiveContractsText();
        System.out.println("Customer Active Contracts Total Records: " + rowCountActiveContractsText);
        Assert.assertNotNull(rowCountCustomerContractsText);
        driver.close();
        driver.quit();
        
    }
}
