package StrikeDeckFramework.src.test.java;

import com.selenium.Pages.StrikeDeckHomePage;
import StrikeDeckFramework.src.main.java.com.selenium.Pages.StrikeDeckLoginPage;
import com.selenium.commonLibs.BaseTest;
import org.junit.Assert;
import org.junit.Test;

public class TC_HomePageValidation_002 extends BaseTest {

    @Test
    public void loginTest() throws Exception {


        StrikeDeckLoginPage sdlp = new StrikeDeckLoginPage(driver);
        sdlp.loginToSD(username,password);
        Thread.sleep(3000);
        driver.navigate().to("https://spear-uat.strikedeck.com/");
        String winHandleBefore = driver.getWindowHandle();
        driver.switchTo().window(winHandleBefore);
        Thread.sleep(1000);
        String title = driver.getTitle();
        System.out.println(title);
        StrikeDeckHomePage homePage = new StrikeDeckHomePage(driver);
        String expectedLogoName = "spear";
        Assert.assertEquals(expectedLogoName,homePage.getaccountLogo());
        driver.quit();
    }

}
