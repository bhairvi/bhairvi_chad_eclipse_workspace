package com.speareducation.commonLibraries;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.selenium.contract.IDropdown;

public class DropDownControl implements IDropdown {
	

//private 
Select getDropDown(WebElement element){
		
		Select select= new Select(element);
		return select;
	}

	@Override
	public void selectViaVisbleText(WebElement element, String visibleText) throws Exception {
	
		getDropDown(element).selectByVisibleText(visibleText);
	}

	@Override
	public void selectViaValue(WebElement element, String value) throws Exception {
		getDropDown(element).selectByValue(value);

	}

	@Override
	public void selectViaIndex(WebElement element, int Index) throws Exception {
		getDropDown(element).selectByIndex(Index);

	}

	@Override
	public boolean isMultiple(WebElement element) throws Exception {
		boolean status= getDropDown(element).isMultiple();
		return status;
	}

	@Override
	public List<WebElement> getAllOption(WebElement element) throws Exception {
		List<WebElement> alloption= getDropDown(element).getOptions();
		return alloption;
	}

	@Override
	public List<WebElement> getAllSelectedOptions(WebElement element) throws Exception {
		List<WebElement> SelectedOption= getDropDown(element).getAllSelectedOptions();
		return SelectedOption;
	}

	@Override
	public WebElement getFirstSelectedOption(WebElement element) throws Exception {
		return getDropDown(element).getFirstSelectedOption();
	}

}
