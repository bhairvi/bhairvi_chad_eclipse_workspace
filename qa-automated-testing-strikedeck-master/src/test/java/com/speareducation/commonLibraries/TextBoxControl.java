package com.speareducation.commonLibraries;

import org.openqa.selenium.WebElement;

import com.selenium.contract.ITextBox;

public class TextBoxControl implements ITextBox{

	@Override
	public void setText(WebElement element, String textToSet) throws Exception {
		element.sendKeys(textToSet);
		
	}

	@Override
	public void clearText(WebElement element) throws Exception {
		element.clear();
		
		
	}

}
