package com.speareducation.commonLibraries;

import java.util.Set;

import org.openqa.selenium.WebDriver;

import com.selenium.contract.IWindowHandle;

public class WindowHandleControls implements IWindowHandle{
	 WebDriver driver;
	 
	 public WindowHandleControls(WebDriver driver) {
		 this.driver = driver;
	 }
	
	 private void switchtowindow(String windowHandle) {
		 
			driver.switchTo().window(windowHandle);
		 }
	
	@Override
	public void switchToChildWindow() throws Exception {
		
		String childwindow = getWindowHandles().toArray()[1].toString();
		switchtowindow(childwindow);
	}
	
	@Override
	public void switchToParentWindow() throws Exception {
		String parentwindow = getWindowHandle();
		switchtowindow(parentwindow);
	}
		
	
	@Override
	public String getWindowHandle() throws Exception {
		
		String windowhandle = driver.getWindowHandle();
		return windowhandle;
	}

	@Override
	public Set<String> getWindowHandles() throws Exception {
		Set<String> childwindow = driver.getWindowHandles();
		return childwindow;
	}

}
