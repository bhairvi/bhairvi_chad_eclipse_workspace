package com.speareducation.commonLibraries;

import com.speareducation.testData.ConfigFileReader;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;


public class BaseTest {

    public static WebDriver driver;
    public static final String USERNAME = "bhairvichad2";
    public static final String AUTOMATE_KEY = "yt4z3CzNyc2TqvaYwY5T";
    public static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";
    //public String baseUrl="https://spear-uat.strikedeck.com/";
    public String username="qa_ns_automation@speareducation.com";
    public String password="20PAssword20";
    public static ConfigFileReader configFileReader = new ConfigFileReader();
    public static String reportName = "Unset-Report-Name";
    //public static Logger logger;

    @BeforeClass
    public static void setup()
    {
        if (configFileReader.getExecutionLocation() == ConfigFileReader.ExecutionLocation.LOCAL) {
            switch (configFileReader.getBrowser()) {
                case CHROME:
                    driver = new ChromeDriver();
                    break;
                case IE:
                    driver = new InternetExplorerDriver();
                    break;
                case FIREFOX:
                    driver = new FirefoxDriver();
                    break;
                case OPERA:
                    driver = new OperaDriver();
                    break;
                case EDGE:
                    driver = new EdgeDriver();
                    break;
            }
        } else {
            LocalDateTime datetime = LocalDateTime.now();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy - HH");
            String date = datetime.format(formatter);
            DesiredCapabilities caps = new DesiredCapabilities();
            caps.setCapability("browser", configFileReader.getBrowser().get());
            caps.setCapability("os", "Windows");
            caps.setCapability("os_version", "10");
            caps.setCapability("resolution", "1920x1080");
            caps.setCapability("project", "NetSuite Automation");
            caps.setCapability("build", date);
            caps.setCapability("name", reportName);
            caps.setCapability("browserstack.local", "false");
        }
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get("oktaURL");
        System.out.println("Okta URL is opened");
        driver.manage().deleteAllCookies();
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        //Logger logger=Logger.getLogger("StrikeDeck");
        //PropertyConfigurator.configure("log4j.properties");
    }

    @AfterClass
    public static void tearDown()
    {
        //driver.quit();
    }
   /* @parameters{"browserType","OS","version","environment","exec       Envirponment"}
    public void configure(String browsertYpe,String os,String ver,String env,String execEnv){

        if(env.equalsIgnoreCase("QA"))
        if(browsertYpe.equalsIgnoreCase("Chrome"))
        {

        }
    }*/
    public void waitforElementVisible(long milliseconds){
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public void doPageScroll(){
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,500)", "");
    }

}
