package com.speareducation.commonLibraries;

import com.selenium.contract.IAlerts;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import com.speareducation.commonFunctions.Waitutils;

public class AlertsControl implements IAlerts {

    WebDriver driver;

    public AlertsControl(WebDriver driver) {
        this.driver = driver;

    }

    private Alert getAlert() {
        Alert alert = driver.switchTo().alert();
        return alert;
    }

    @Override
    public void acceptingAlert() throws Exception {

        getAlert().accept();
    }

    @Override
    public void rejectingAlert() throws Exception {


        getAlert().dismiss();
    }

    @Override
    public String getMessageFromAlert() throws Exception {
        String message = getAlert().getText();
        return message;


    }

    @Override
    public boolean isAlertPresent(long timeOutinSeconds) throws Exception {
        Waitutils.waitTillAlertIsPresent(driver, timeOutinSeconds);
        return true;
    }
}
	