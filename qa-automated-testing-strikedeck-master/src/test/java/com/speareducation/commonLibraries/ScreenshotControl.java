package com.speareducation.commonLibraries;

import java.io.File;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.io.FileHandler;

import com.selenium.contract.Iscreenshots;

public class ScreenshotControl implements Iscreenshots{
	
	private WebDriver driver;
	
	public ScreenshotControl(WebDriver driver) {
		this.driver = driver;
	}
	
	

	@Override
	public void captureandsavescreenshot(String fileName ) throws Exception {
		fileName= fileName.trim();
		
		File ImgFile, TmpFile;
		
		ImgFile=  new File(fileName);
		if(ImgFile.exists()) {
			throw new Exception ("File Already Exits");
		}
		
		TakesScreenshot Screenshot;
		Screenshot = (TakesScreenshot) driver;
		
		
		TmpFile= Screenshot.getScreenshotAs(OutputType.FILE);
		FileHandler.copy(TmpFile, ImgFile);
		
	}

}
