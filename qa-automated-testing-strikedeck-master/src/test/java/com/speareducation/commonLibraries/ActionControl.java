package com.speareducation.commonLibraries;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.selenium.contract.IActions;

public class ActionControl implements IActions
{

	WebDriver driver;
	
	public ActionControl(WebDriver driver) {
		this.driver= driver;
		
	}
	
	private Actions getActions() {
		
	Actions action = new Actions(driver);
	return action;

}

	@Override
	public void dragAndDrop(WebElement element1, WebElement element2) throws Exception {
		getActions().dragAndDrop(element1, element2).build().perform();
		
	}

	@Override
	public void moveToElement(WebElement element) throws Exception {
		getActions().moveToElement(element).build().perform();
		
	}

	@Override
	public void rightClick(WebElement element) throws Exception {
	getActions().contextClick().build().perform();	
		
	}

	@Override
	public void doubleClick(WebElement element) throws Exception {
	getActions().doubleClick().build().perform();	
	}

	@Override
	public void moveToElementAndClick(WebElement element) throws Exception {
	getActions().moveToElement(element).click().build().perform();
		
	}

}
