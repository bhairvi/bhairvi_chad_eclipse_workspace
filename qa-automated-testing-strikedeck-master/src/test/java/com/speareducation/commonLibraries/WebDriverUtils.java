package com.speareducation.commonLibraries;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class WebDriverUtils {

    public void click(WebElement element) {
        element.click();
    }


    public String getText(WebElement element) {
        String text = element.getText();
        return text;
    }


    public String getAttribute(WebElement element, String Attribute) {
        String Attributevalue = element.getAttribute(Attribute);
        return Attributevalue;
    }


    public String getCssValue(WebElement element, String csspropertyName) {
        String cssvalue = element.getCssValue(csspropertyName);
        return cssvalue;
    }


    public boolean isElementEnabled(WebElement element) {
        boolean status = element.isEnabled();
        return status;
    }


    public boolean isElementVisble(WebElement element) {
        boolean status = element.isDisplayed();
        return status;
    }


    public boolean isElementSelected(WebElement element) throws Exception {
        boolean status = element.isSelected();
        return status;
    }


    public void enterText(WebElement element, String testData) {
        element.sendKeys(testData);
    }

    public void pressEnterKey(WebElement element) {
        element.sendKeys(Keys.ENTER);
    }


}



