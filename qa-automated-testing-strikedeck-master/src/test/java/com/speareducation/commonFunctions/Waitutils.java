package com.speareducation.commonFunctions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Waitutils {
	
	public static WebDriverWait getWait(WebDriver driver, long timeOutinSeconds) {
		WebDriverWait wait = new WebDriverWait(driver, timeOutinSeconds);
		return wait;
	}
	
	public static void waitTillAlertIsPresent(WebDriver driver, long timeOutinSeconds) {
    
		getWait(driver, timeOutinSeconds).until(ExpectedConditions.alertIsPresent());
		
	}
public static void waitTillElementIsvisible(WebDriver driver, long timeOutinSeconds, WebElement element) {
	
		getWait(driver, timeOutinSeconds).until(ExpectedConditions.visibilityOf(element));
		
}
	
}
