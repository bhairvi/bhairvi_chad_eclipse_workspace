package com.speareducation.pages;
import com.speareducation.commonLibraries.WebDriverUtils;
import com.speareducation.commonLibraries.WebElementsControl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CustomerWesitePage {
    WebDriver ldriver;
    public CustomerWesitePage(WebDriver rdriver){
        ldriver = rdriver;
        PageFactory.initElements(rdriver, this);
    }
       WebDriverUtils webDriverUtils = new WebDriverUtils();


    @FindBy(xpath = "//img[@alt='Woodbury Dental Arts - Dr. Kamel - Woodbury, MN']")
    WebElement websiteLogo;

    public String getwebsiteLogo() throws Exception
    {
        return webDriverUtils.getAttribute(websiteLogo,"alt");

    }
}
