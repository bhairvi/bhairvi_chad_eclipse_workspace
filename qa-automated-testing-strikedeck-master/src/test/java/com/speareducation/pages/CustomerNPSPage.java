package com.speareducation.pages;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.speareducation.commonLibraries.WebDriverUtils;

public class CustomerNPSPage {
    WebDriver ldriver;

    public CustomerNPSPage(WebDriver rdriver) {
        ldriver = rdriver;
        PageFactory.initElements(rdriver, this);
    }

    WebDriverUtils webDriverUtils = new WebDriverUtils();

    @FindBy(xpath = "//*[@id=\"pod_164_5ec5a8967d3f5f3b1c8bbd3d\"]//div[1]/h4")
    WebElement customerNPSScoreResponsesPodHeaderText;
    @FindBy(xpath = "//*[@id=\"SurveyResponseBasedTable_5ec5a8967d3f5f3b1c8bbd3d\"]//td")
    WebElement recordsText1;
    @FindBy(xpath = "//*[@id=\"pod_161_5ec472167d3f5f3927bb0441\"]//div[1]/h4")
    WebElement customerNPSScoreLast365daysPodHeaderText;
    @FindBy(xpath = "//*[@id=\"CustomerBasedTable_5ec472167d3f5f3927bb0441\"]//tr/td ")
    WebElement recordsText2;


    public String getcustomerNPSScoreResponsesPodHeaderText() throws Exception
    {
        return webDriverUtils.getText(customerNPSScoreResponsesPodHeaderText);

    }
    public String getrecordsText1() throws Exception
    {
        return webDriverUtils.getText(recordsText1);

    }
    public String getcustomerNPSScoreLast365daysPodHeaderText() throws Exception
    {
        return webDriverUtils.getText(customerNPSScoreLast365daysPodHeaderText);

    }
    public String getrecordsText2() throws Exception
    {
        return webDriverUtils.getText(recordsText2);

    }

}
