package com.speareducation.pages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.speareducation.commonLibraries.BaseTest;

public class LoginPage {
    WebDriver driver;
    //public static String email = "email";
    //public static String pwd = "password";

    public static By userName = By.id("okta-signin-username");
    By password = By.id("okta-signin-password");
    By loginButton = By.id("okta-signin-submit");

    //Constructor for class
    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    //Set user name in field
    public void setUserName(String strUserName) {
        System.out.println("Inserting username....");
        driver.findElement(userName).sendKeys(strUserName);
    }

    //Set Password in field
    public void setPassword(String strPassword) {
        System.out.println("Inserting password....");
        driver.findElement(password).sendKeys(strPassword);
    }

    //Select the login button
    public void clickLogin() {
        System.out.println("Logging in....");
        driver.findElement(loginButton).click();
    }

    public void loginToStrikeDeck(String strUserName, String strPasword) {

        //Fill user name
        this.setUserName(strUserName);

        //Fill password
        this.setPassword(strPasword);

        //Click Login button
        this.clickLogin();

    }
}

