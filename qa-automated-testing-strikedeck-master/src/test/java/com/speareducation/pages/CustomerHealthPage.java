package com.speareducation.pages;

import com.speareducation.commonLibraries.WebDriverUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CustomerHealthPage {
    WebDriver ldriver;

    public CustomerHealthPage(WebDriver rdriver) {
        ldriver = rdriver;
        PageFactory.initElements(rdriver, this);
    }

    WebDriverUtils webDriverUtils = new WebDriverUtils();

    @FindBy(xpath = "//*[@id=\"pod_186_5ee92a9f98b22c0709b87869\"]//div[1]/h4")
    WebElement customerHealthMetricDrillDownPodHeaderText;
    @FindBy(xpath = "/html/body/div[1]/div[2]/section[7]/div/div/div/div/div/div[3]/div/div/div[2]/div")
    WebElement textWarningText;


    public String getcustomerHealthMetricDrillDownPodHeaderText() throws Exception {
        return webDriverUtils.getText(customerHealthMetricDrillDownPodHeaderText);

    }

    public String gettextWarningText() throws Exception {
        return webDriverUtils.getText(textWarningText);

    }

}

