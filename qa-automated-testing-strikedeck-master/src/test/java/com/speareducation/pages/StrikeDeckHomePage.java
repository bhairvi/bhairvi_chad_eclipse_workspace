package com.speareducation.pages;

import com.speareducation.commonLibraries.WebDriverUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class StrikeDeckHomePage {
    WebDriver ldriver;

    public StrikeDeckHomePage(WebDriver rdriver) {
        ldriver = rdriver;
        PageFactory.initElements(rdriver, this);
    }

    WebDriverUtils webDriverUtils = new WebDriverUtils();

    @FindBy(xpath = "//a/i/span[contains(text(),'Customer 360')]")
    WebElement strikeDeckHomeTitle;

    @FindBy(xpath = "//img[@alt='spear']")
    WebElement accountLogo;

    @FindBy(xpath = "//a[@id='pills-5d9898c63b24d3111dabd17c-tab']")
    WebElement customer360TasksTab;

    @FindBy(xpath = "//input[@name='customerSearchInNavbar']")
    WebElement customerSearchTxtField;

    @FindBy(xpath = "//tr[@data-index='0']/td[3]/a")
    WebElement customerName;

    @FindBy(id = "pills-5d9898c73b24d3111dabd1c5-tab")
    WebElement customerSummaryTab;

    @FindBy(xpath = "//h4[contains(text(),'Spear Online')]")
    WebElement spearonlinetracker;

    @FindBy(xpath = "//a[contains(@href, '#pills-5da968f53b24d32e8d17d48b')]")
    WebElement spearOnlineTab;

    public String getstrikeDeckHomeTitle() throws Exception {
        return webDriverUtils.getText(strikeDeckHomeTitle);
//has to return here and store value in testcase to get the extract.with assertion
    }

    public String getaccountLogo() throws Exception {
        return webDriverUtils.getAttribute(accountLogo, "alt");

    }

    public String getcustomerName() throws Exception {
        return webDriverUtils.getText(customerName);
    }

    public void enterCustomerName(String customerName) throws Exception {
        webDriverUtils.enterText(customerSearchTxtField, customerName);
    }

    public void enterSearchCustomerName() throws Exception {
        customerSearchTxtField.sendKeys("Marko Kamel");
    }
       /* public void sendKeysCustomerName
    public void pressEnterKey(WebElement element ) {
        element.sendKeys(Keys.ENTER);
    }*/

    public void enterTextForCustomerName(String text) {
        customerSearchTxtField.sendKeys("Makr Kamel");
    }

    public void clickEnter() {
        webDriverUtils.pressEnterKey(customerSearchTxtField);
    }

    public void clickcustomerSummaryTab() throws Exception {
        webDriverUtils.click(customerSummaryTab);
    }

    //Customer360 Tabs
    public void clickCustomer360TasksTab() throws Exception {
        webDriverUtils.click(customer360TasksTab);
    }

    //SpearOnline tab Tracker *done
    public void clickspearOnlineTab() throws Exception {
        webDriverUtils.click(spearOnlineTab);
    }

    //spear online tracker header name
    public void getspearonlinetracker() throws Exception {
        webDriverUtils.getText(spearonlinetracker);
    }


    public String getcustomerSearchTitle() throws Exception {
        //Fill user name
        String extractcustomerName = getcustomerName();
        System.out.println("Customer Name extracted");
        enterCustomerName(extractcustomerName);
        System.out.println("Customer Name Entered in CustomerSearch");
        Thread.sleep(3000);
        clickEnter();
        Thread.sleep(2000);
        System.out.println("User is in Customers details Page");
        return ldriver.getTitle();


    }

    public String getSearchexistingcustomer() throws Exception {
        //Fill user name
        enterSearchCustomerName();
        System.out.println("Customer Name Marko Kamel Entered in CustomerSearch");
        Thread.sleep(3000);
        clickEnter();
        Thread.sleep(2000);
        System.out.println("User is in Customers details Page");
        Thread.sleep(2000);
        return ldriver.getTitle();
        //String title = ldriver.getTitle();
        // return title;

    }
}