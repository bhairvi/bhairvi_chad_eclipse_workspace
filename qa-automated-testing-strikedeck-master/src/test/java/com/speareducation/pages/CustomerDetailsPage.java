package com.speareducation.pages;

import com.speareducation.commonLibraries.WebElementsControl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CustomerDetailsPage {
    WebDriver ldriver;
    public CustomerDetailsPage(WebDriver rdriver){
        ldriver = rdriver;
        PageFactory.initElements(rdriver, this);
    }
    WebElementsControl elementcontrol = new WebElementsControl();

    @FindBy(id = "pills-5d9898c73b24d3111dabd1c5-tab")
    WebElement summaryTab;
    @FindBy(id = "pills-5d9898c73b24d3111dabd1e7-tab")
    WebElement contactsTab;
    @FindBy(id = "pills-5e13a28a3b24d35a383d020b-tab")
    WebElement activitiesTab;
    @FindBy(id = "pills-5d9898c73b24d3111dabd1ed-tab")
    WebElement tasksTab;
    @FindBy(id = "pills-5d9898c73b24d3111dabd1f3-tab")
    WebElement notesTab;
    @FindBy(id = "pills-5dfd256e3b24d33bc54d8708-tab")
    WebElement CampusTab;
    @FindBy(id = "pills-5e18c29e3b24d36e75d9e299-tab")
    WebElement contactHistoryTab;
    @FindBy(id = "pills-5e7a46f93b24d3768f2e3783-tab")
    WebElement customerHealthTab;
    @FindBy(id = "pills-5ec470037d3f5f3927baf237-tab")
    WebElement npsTab;
    @FindBy(id = "pills-5f365f1da0c522128231adb4-tab")
    WebElement journeyTab;

    public void clicksummaryTab() throws Exception
    {
        elementcontrol.click(summaryTab);
    }
    public void clickcontactsTab() throws Exception
    {
        elementcontrol.click(contactsTab);
    }
    public void clickactivitiesTab() throws Exception
    {
        elementcontrol.click(activitiesTab);
    }
    public void clicktasksTab() throws Exception
    {
        elementcontrol.click(tasksTab);
    }
    public void clicknotesTab() throws Exception
    {
        elementcontrol.click(notesTab);
    }
    public void clickcontactHistoryTab() throws Exception
    {
        elementcontrol.click(contactHistoryTab);
    }
    public void clickcustomerHealthTab() throws Exception
    {
        elementcontrol.click(customerHealthTab);
    }
    public void clicknpsTab() throws Exception
    {
        elementcontrol.click(npsTab);
    }
    public void clickjourneyTab() throws Exception
    {
        elementcontrol.click(journeyTab);
    }
    public void clickCampusTab() throws Exception
    {
        elementcontrol.click(CampusTab);
    }
}
