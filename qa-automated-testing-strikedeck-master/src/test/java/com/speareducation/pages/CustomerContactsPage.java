package com.speareducation.pages;

import com.speareducation.commonLibraries.WebDriverUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class CustomerContactsPage implements WebElement {
    WebDriver ldriver;

    public CustomerContactsPage(WebDriver rdriver) {
        ldriver = rdriver;
        PageFactory.initElements(rdriver, this);
    }

    WebDriverUtils webDriverUtils = new WebDriverUtils();

    @FindBy(xpath = "//h4[contains(text(),'Contacts')]")
    WebElement customerContactsPodHeaderText;

    @FindBy(xpath = "//*[@id='pod_16_5d9898c63b24d3111dabd104']//div[3]/div[1]/span[1]")
    WebElement rowCountCustomerContactsText;

    @FindBy(xpath = "//*[@id=\"contactsTable\"]//button")
    WebElement roleDropdownArrow;

    @FindBy(xpath = "//*[@id=\"contactsTable\"]//li[1]")
     WebElement selectAll;

    @FindBy(xpath = "//*[@id=\"contactsTable\"]//li[2]/a/label")
    WebElement defaultContact;

    @FindBy(xpath = "//*[@id=\"contactsTable\"]//li[8]/a/label")
    WebElement frontOfficeTeam;

    @FindBy(xpath = "//*[@id=\"contactsTable\"]//li[11]/a/label")
    WebElement hygienist;


    public String getcustomerContactsPodHeaderText() throws Exception {
        return webDriverUtils.getText(customerContactsPodHeaderText);
    }
    public String getrowCountCustomerContactsText() throws Exception
    {
        return webDriverUtils.getText(rowCountCustomerContactsText);

    }
    public void clickroleDropdownArrow() throws Exception{
        webDriverUtils.click(roleDropdownArrow);
    }
    public void clickselectAll() throws Exception{
        webDriverUtils.click(selectAll);
    }
    public void clickdefaultContact() throws Exception{
        webDriverUtils.click(defaultContact);
    }
    public void clickfrontOfficeTeam() throws Exception{
        webDriverUtils.click(frontOfficeTeam);
    }
    public void clickhygienist() throws Exception{
        webDriverUtils.click(hygienist);
    }
    @Override
    public void click() {

    }

    @Override
    public void submit() {

    }

    @Override
    public void sendKeys(CharSequence... keysToSend) {

    }

    @Override
    public void clear() {

    }

    @Override
    public String getTagName() {
        return null;
    }

    @Override
    public String getAttribute(String name) {
        return null;
    }

    @Override
    public boolean isSelected() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }

    @Override
    public String getText() {
        return null;
    }

    @Override
    public List<WebElement> findElements(By by) {
        return null;
    }

    @Override
    public WebElement findElement(By by) {
        return null;
    }

    @Override
    public boolean isDisplayed() {
        return false;
    }

    @Override
    public Point getLocation() {
        return null;
    }

    @Override
    public Dimension getSize() {
        return null;
    }

    @Override
    public Rectangle getRect() {
        return null;
    }

    @Override
    public String getCssValue(String propertyName) {
        return null;
    }

    @Override
    public <X> X getScreenshotAs(OutputType<X> target) throws WebDriverException {
        return null;
    }
}
