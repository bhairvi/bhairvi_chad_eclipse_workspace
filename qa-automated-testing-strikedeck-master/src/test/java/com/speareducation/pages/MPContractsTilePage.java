package com.speareducation.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.speareducation.commonLibraries.WebDriverUtils;

public class MPContractsTilePage     {
        WebDriver ldriver;
    public MPContractsTilePage(WebDriver rdriver) {
        ldriver = rdriver;
        PageFactory.initElements(rdriver, this);
    }

        WebDriverUtils webDriverUtils = new WebDriverUtils();

        @FindBy(xpath = "//*[@id=\"tileHelperWithBuilder_2f1423ae-b461-4767-b1c8-87b371dfaa5d\"]//div/h2")
        WebElement MPActiveContractsPodHeaderText;
        @FindBy(xpath = "/html/body/div[1]/section/div/div/div/div[1]/h4")
        WebElement activeContractsDrilldownHeaderText;
        @FindBy(className = "pagination-info")
        WebElement rowCountactiveContracts;

        @FindBy(xpath = "//*[@id=\"segmentResultTable\"]//th[3]/div[2]/div/span/div/button")
        WebElement typeofOrderDropdown;
        @FindBy(xpath = "//*[@id=\"segmentResultTable\"]/thead/tr/th[3]/div[2]/div/span/div/ul/li[2]/a/label")
        WebElement typeofOrderNew;
        @FindBy(xpath = "//*[@id=\"segmentResultTable\"]//th[3]/div[2]/div/span/div/ul/li[3]/a/label")
        WebElement typeofOrderPoachIn;
        @FindBy(xpath = "//*[@id=\"segmentResultTable\"]//th[3]/div[2]/div/span/div/ul/li[4]/a/label")
        WebElement typeofOrderRenewal;
        @FindBy(id = "sidebar_customer360")
        WebElement customer360;

        @FindBy(xpath = "//*[@id=\"tileHelperWithBuilder_2f1423ae-b461-4767-b1c8-87b371dfaa5d\"]//ul/li[1]")
        WebElement renewNext90daysContractsPodHeaderText;
        @FindBy(className = "pagination-info")
        WebElement rowCountrenewNext90daysContracts;
        @FindBy(xpath = "/html/body/div[1]/section/div/div/div/div[1]/h4")
        WebElement renewNext90daysContractsDrilldownHeaderText;

        @FindBy(xpath = "//*[@id=\"tileHelperWithBuilder_2f1423ae-b461-4767-b1c8-87b371dfaa5d\"]//ul/li[2]")
        WebElement renewLast60daysContractsPodHeaderText;
        @FindBy(className = "pagination-info")
        WebElement rowCountrenewLast60daysContracts;
        @FindBy(xpath = "/html/body/div[1]/section/div/div/div/div[1]/h4")
        WebElement renewLast60daysontractsDrilldownHeaderText;



        public void clickMPActiveContractsPodHeaderText() throws Exception {
        webDriverUtils.click(MPActiveContractsPodHeaderText);
    }

        public String getactiveContractsDrilldownHeaderText() throws Exception {
        return webDriverUtils.getText(activeContractsDrilldownHeaderText);
    }
        public String getrowCountactiveContracts() throws Exception {
        return webDriverUtils.getText(rowCountactiveContracts);
    }
        public void clicktypeofOrderDropdown() throws Exception {
        webDriverUtils.click(typeofOrderDropdown);
    }
        public void clicktypeofOrderNew() throws Exception {
        webDriverUtils.click(typeofOrderNew);
    }
        public void clicktypeofOrderPoachIn() throws Exception {
        webDriverUtils.click(typeofOrderPoachIn);
    }
        public void clicktypeofOrderRenewal() throws Exception {
        webDriverUtils.click(typeofOrderRenewal);
    }
        public void clickcustomer360() throws Exception {
        webDriverUtils.click(customer360);
    }
        public void clickrenewNext90daysContractsPodHeaderText() throws Exception {
        webDriverUtils.click(renewNext90daysContractsPodHeaderText);
    }
        public String getrowCountrenewNext90daysContracts() throws Exception {
        return webDriverUtils.getText(rowCountrenewNext90daysContracts);
    }
        public String getrenewNext90daysContractsDrilldownHeaderText() throws Exception {
        return webDriverUtils.getText(renewNext90daysContractsDrilldownHeaderText);
    }
        public void clickrenewLast60daysContractsPodHeaderText() throws Exception {
        webDriverUtils.click(renewLast60daysContractsPodHeaderText);
    }
        public String getrowCountrenewLast60daysContracts() throws Exception {
        return webDriverUtils.getText(rowCountrenewLast60daysContracts);
    }
        public String getrenewLast60daysontractsDrilldownHeaderText() throws Exception {
        return webDriverUtils.getText(renewLast60daysontractsDrilldownHeaderText);
    }

}
