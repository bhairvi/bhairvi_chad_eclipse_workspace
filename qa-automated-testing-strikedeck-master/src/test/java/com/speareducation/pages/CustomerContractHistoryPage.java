package com.speareducation.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.speareducation.commonLibraries.WebDriverUtils;

public class CustomerContractHistoryPage {

    WebDriver ldriver;

    public CustomerContractHistoryPage(WebDriver rdriver) {
        ldriver = rdriver;
        PageFactory.initElements(rdriver, this);
    }

   WebDriverUtils webDriverUtils = new WebDriverUtils();

    @FindBy(xpath = "//h4[contains(text(),'Contract History')]")
    WebElement contractHistoryPodHeaderText;
    @FindBy(xpath = "/html/body/div[1]/div[2]/section[7]/div/div/div[2]/div/div/div[3]/div/div/div[1]/h4")
    WebElement customerContractPodHeaderText;
    @FindBy(xpath = "//*[@id=\"pod_121_5e18d40f3b24d36e75da3923\"]//div[3]/div[1]/span[1]")
    WebElement rowCountContractHistoryPodText;
    @FindBy(xpath = "//html/body/div[1]/div[2]/section[7]/div/div/div[2]/div/div/div[3]/div/div/div[2]/div[1]/div[3]/div[1]/span[1]")
    WebElement rowCountCustomerContractPodText;
    @FindBy(xpath = "//*[@id=\"ContractBasedTable_5e18d40f3b24d36e75da3923\"]//th[5]/div[2]/div/span/div/ul/li[1]/a/label/input")
    WebElement selectAllCheckBox;
    @FindBy(xpath = "//*[@id=\"ContractBasedTable_5e18d40f3b24d36e75da3923\"]//div[2]/div/span/div/ul/li[5]/a/label")
    WebElement cDoCSResidentCheckBox;

    public String getcontractHistoryPodHeaderText() throws Exception {
        return webDriverUtils.getText(contractHistoryPodHeaderText);
    }
     public String getcustomerContractPodHeaderText() throws Exception {
        return webDriverUtils.getText(customerContractPodHeaderText);
    }
    public String getrowCountContractHistoryPodText() throws Exception {
        return webDriverUtils.getText(rowCountContractHistoryPodText);
    }
    public String getrowCountCustomerContractPodText() throws Exception {
        return webDriverUtils.getText(rowCountCustomerContractPodText);
    }
    public void clickselectAllCheckBox() throws Exception {
        webDriverUtils.click(selectAllCheckBox);

    }
    public void clickcDoCSResidentCheckBox() throws Exception {
        webDriverUtils.click(cDoCSResidentCheckBox);

    }
}

