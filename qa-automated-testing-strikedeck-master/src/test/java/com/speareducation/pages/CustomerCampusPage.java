package com.speareducation.pages;

import com.speareducation.commonLibraries.WebDriverUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CustomerCampusPage {
    WebDriver ldriver;

    public CustomerCampusPage(WebDriver rdriver) {
        ldriver = rdriver;
        PageFactory.initElements(rdriver, this);
    }

    WebDriverUtils webDriverUtils = new WebDriverUtils();

    @FindBy(xpath = "//*[@id=\"pod_155_5e98e6623b24d31cc90a5af2\"]//div[1]/h4")
    WebElement customerCourseAttendancePodHeaderText;
    @FindBy(xpath = "//*[@id=\"pod_155_5e98e6623b24d31cc90a5af2\"]//div[3]/div[1]/span[1]")
    WebElement rowCountcustomerCourseAttendance;
    @FindBy(xpath = "//*[@id=\"pod_95_5d9f07f13b24d3111d1143ce\"]//div[1]/h4")
    WebElement upcomingCourseAttendancePodHeaderText;
    @FindBy(xpath = "//*[@id=\"Course_Attendance__cBasedTable_5d9f07f13b24d3111d1143ce\"]/tbody/tr")
    WebElement rowCountupcomingCourseAttendance;

    public String getcustomerCourseAttendancePodHeaderText() throws Exception {
        return webDriverUtils.getText(customerCourseAttendancePodHeaderText);

    }

    public String getrowCountcustomerCourseAttendance() throws Exception {
        return webDriverUtils.getText(rowCountcustomerCourseAttendance);

    }
}

