package com.speareducation.pages;
//package StrikeDeckFramework.src.main.java.com.selenium.Pages;

import com.speareducation.commonLibraries.WebDriverUtils;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CustomerSummaryPage {
    WebDriver ldriver;

    public CustomerSummaryPage(WebDriver rdriver) {
        ldriver = rdriver;
        PageFactory.initElements(rdriver, this);
    }

    WebDriverUtils webDriverUtils = new WebDriverUtils();

    @FindBy(xpath = "//h4[contains(text(),'General Information')]")
    WebElement generalInformationHeaderText;
    @FindBy(xpath = "//h4[contains(text(),'Address Information')]")
    WebElement addressInformationHeaderText;
    @FindBy(xpath = "//h4[contains(text(),'Customer Personnel')]")
    WebElement customerPersonnelHeaderText;
    @FindBy(xpath = "//h4[contains(text(),'Seminar Vouchers')]")
    WebElement seminarVocherPodHeaderText;
    @FindBy(xpath = "//h4[contains(text(),'Customer Contracts')]")
    WebElement customerContractsPodHeaderText;
    @FindBy(xpath = "//a[contains(@href, 'https://859995-sb3.app.netsuite.com/app/common/entity/custjob.nl?id=69452')]")
    WebElement externalIdLink;//class='fal fa-location-arrow cursorPointer linkTo_externalId';
    @FindBy(xpath = "//a[@href='mailto:markokamel@cs.com']") //Class='fal fa-location-arrow cursorPointer linkTo_email__c';
    WebElement emailLink;
    @FindBy(xpath = "//a[@href='http://woodburydentalarts.com']")//class="fal fa-location-arrow cursorPointer linkTo_website"
    WebElement websiteLink;
    @FindBy(className = "pagination-info")
    WebElement rowCountSeminarVocherText;
    @FindBy(xpath = "//*[@id=\"pod_121_5e18d40f3b24d36e75da3923\"]//div[3]/div[1]/span[1]")
    WebElement rowCountCustomerContractsText;
    @FindBy(xpath = "//*[@id=\"pod_93_5d9b4a183b24d3111de3e88b\"]//div[3]/div[1]/span[1]")
    WebElement rowCountActiveContractsText;
    //WebElement Rowcount= driver.findElement(By.xpath("//*[@id='studyClubPanel]/div[11]/div[2]/div[4]/div[1]/span[1]"));
    //String Attribute = null;
    //	elementcontrol.getAttribute(Rowcount, Attribute);

    public String getgeneralInformationHeaderText() throws Exception {
        return webDriverUtils.getText(generalInformationHeaderText);
    }

    public String getaddressInformationHeaderText() throws Exception {
        return webDriverUtils.getText(addressInformationHeaderText);
    }

    public String getcustomerPersonnelHeaderText() throws Exception {
        return webDriverUtils.getText(customerPersonnelHeaderText);
    }

    public String getseminarVocherPodHeaderText() throws Exception {
        return webDriverUtils.getText(seminarVocherPodHeaderText);
    }

    public String getcustomerContractsPodHeaderText() throws Exception {
        return webDriverUtils.getText(customerContractsPodHeaderText);
    }

    public String clickexternalIdLink() throws Exception {
        webDriverUtils.click(externalIdLink);
        return webDriverUtils.getAttribute(externalIdLink, "title");
    }

    public void clickemailLink() throws Exception {
        webDriverUtils.click(emailLink);
        //eturn webDriverUtils.getAttribute(emailLink,"title");
    }

    public String clickwebsiteLink() throws Exception {
        webDriverUtils.click(websiteLink);
        return webDriverUtils.getAttribute(websiteLink, "title");
    }

    public String getrowCountSeminarVocherText() throws Exception {
        return webDriverUtils.getText(rowCountSeminarVocherText);

    }

    public String getrowCountCustomerContractsText() throws Exception {
        return webDriverUtils.getText(rowCountCustomerContractsText);

    }

    public void getHeaderTexts() throws Exception {
        //Get Header Text
        //String extractcustomerName = getcustomerName();
        String generalInformationHeaderText = getgeneralInformationHeaderText();
        System.out.println(generalInformationHeaderText);
       // Assert.assertTrue(generalInformationHeaderText.contains("General "));

        //Assert.assertTrue(String.valueOf(true), generalInformationHeaderText.contains("General "));
        Thread.sleep(3000);

        String addressInformationHeaderText = getaddressInformationHeaderText();
        System.out.println(addressInformationHeaderText);
        //Assert.assertTrue(String.valueOf(true),addressInformationHeaderText.contains("Address |")  );
        //Assert.assertTrue(addressInformationHeaderText.contains("Address |")  );

        Thread.sleep(4000);

        String customerPersonnelHeaderText = getcustomerPersonnelHeaderText();
        System.out.println(customerPersonnelHeaderText);
        //Assert.assertTrue(String.valueOf(true),customerPersonnelHeaderText.contains("Customer |")  );
        //Assert.assertTrue(customerPersonnelHeaderText.contains("Customer |")  );
        Thread.sleep(4000);

    }
    public String getrowCountActiveContractsText() throws Exception {
        return webDriverUtils.getText(rowCountActiveContractsText);

    }
}
