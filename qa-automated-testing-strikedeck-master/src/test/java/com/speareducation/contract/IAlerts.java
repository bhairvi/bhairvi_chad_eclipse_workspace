package com.selenium.contract;

public interface IAlerts {

	public void acceptingAlert() throws Exception;
	
	public void rejectingAlert() throws Exception;
	
	public String getMessageFromAlert() throws Exception;
	
	public boolean isAlertPresent(long timeOutinSeconds) throws Exception;
	
}