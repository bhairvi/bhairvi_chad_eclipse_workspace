package com.speareducation.tests;

import com.speareducation.commonLibraries.BaseTest;
import com.speareducation.pages.StrikeDeckHomePage;
import com.speareducation.pages.StrikeDeckLoginPage;
import org.junit.Assert;
import org.junit.Test;

public class TC_HomePageValidation_002 extends BaseTest {

    @Test
    public void loginTest() throws Exception {


        StrikeDeckLoginPage sdlp = new StrikeDeckLoginPage(driver);
        sdlp.loginToSD(username,password);
        Thread.sleep(3000);
        driver.navigate().to("https://spear-uat.strikedeck.com/");
        String winHandleBefore = driver.getWindowHandle();
        driver.switchTo().window(winHandleBefore);
        Thread.sleep(1000);
        String title = driver.getTitle();
        System.out.println(title);
        StrikeDeckHomePage homePage = new StrikeDeckHomePage(driver);
        String expectedLogoName = "spear";
        Assert.assertEquals(expectedLogoName,homePage.getaccountLogo());
        driver.quit();
    }

}
