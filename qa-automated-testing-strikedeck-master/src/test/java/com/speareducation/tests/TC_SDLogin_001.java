package com.speareducation.tests;

import com.speareducation.commonLibraries.BaseTest;
import com.speareducation.pages.StrikeDeckLoginPage;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


public class TC_SDLogin_001 extends BaseTest {

    @Test
    public void loginTest() throws Exception {


        StrikeDeckLoginPage sdlp = new StrikeDeckLoginPage(driver);
        sdlp.loginToSD(username,password);
        String winHandleBefore = driver.getWindowHandle();
        Thread.sleep(10000);
        // String winHandleBefore = driver.getWindowHandle();
       // driver.switchTo().window(winHandleBefore);


        Set<String> winProps=driver.getWindowHandles();
        List<String> childWindowProps=new ArrayList(winProps);
        driver.switchTo().window(childWindowProps.get(1));
        driver.navigate().to("https://spear-uat.strikedeck.com/");
        //Opening New window for Netsuite.

//sample
        driver.switchTo().window(winHandleBefore);
        System.out.println(childWindowProps.size());
        sdlp.clickNetsuiteURL();
        System.out.println("Netsuite Stage Button clicked");
        winProps=driver.getWindowHandles();
        childWindowProps=new ArrayList(winProps);
        System.out.println(childWindowProps.size());
       // driver.switchTo().window(childWindowProps.get(2));
        driver.switchTo().window(childWindowProps.get(1));
        Thread.sleep(1000);
        driver.switchTo().window(childWindowProps.get(2));
        driver.quit();


    }

}
