package com.speareducation.tests;

import com.speareducation.commonLibraries.BaseTest;
import com.speareducation.pages.CustomerDetailsPage;
import com.speareducation.pages.CustomerSummaryPage;
import com.speareducation.pages.StrikeDeckHomePage;
import com.speareducation.pages.StrikeDeckLoginPage;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class TC_CustomerSummaryTabEmailIdLink_008 extends BaseTest {
    @Test
    public void CustomerSummaryTabEmailIdLink() throws Exception {
        System.out.println("Customer Summary Tab EmailID to Outlook Test begins...");
        StrikeDeckLoginPage sdlp = new StrikeDeckLoginPage(driver);
        sdlp.loginToSD(username,password);
        String winHandleBefore = driver.getWindowHandle();
        Thread.sleep(3000);

        Set<String> winProps = driver.getWindowHandles();
        List<String> childWindowProps = new ArrayList(winProps);
        driver.switchTo().window(childWindowProps.get(1));
        driver.navigate().to("https://spear-uat.strikedeck.com/");

        StrikeDeckHomePage homePage = new StrikeDeckHomePage(driver);
        homePage.clickCustomer360TasksTab();
        waitforElementVisible(1000);
        homePage.clickspearOnlineTab();
        waitforElementVisible(1000);

        homePage.getSearchexistingcustomer();
        Assert.assertTrue(driver.getTitle().contains("Strikedeck |"));



        CustomerDetailsPage summaryTab = new CustomerDetailsPage(driver);
        summaryTab.clicksummaryTab();
        waitforElementVisible(4000);

        CustomerSummaryPage summaryPage = new CustomerSummaryPage(driver);
        waitforElementVisible(4000);
        String generalInformationHeaderText = summaryPage.getgeneralInformationHeaderText();
        System.out.println(generalInformationHeaderText);
        waitforElementVisible(4000);

        summaryPage.clickemailLink();
        System.out.println("Email Link Has been clicked");
        Thread.sleep(5000);
        String parentWindow = driver.getWindowHandle();
        Set<String> allwindows= driver.getWindowHandles();

        for (String childWindow : allwindows) {
            if(!childWindow.equals(parentWindow))
            {
                driver.switchTo().window(childWindow);
                System.out.println(driver.getTitle());
                driver.close();

            }
        }
        //driver.switchTo().window(parentWindow);
        driver.quit();

    }

}
