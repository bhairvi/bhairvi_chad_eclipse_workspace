package com.speareducation.tests;

import com.speareducation.commonLibraries.BaseTest;
import com.speareducation.pages.CustomerDetailsPage;
import com.speareducation.pages.CustomerSummaryPage;
import com.speareducation.pages.StrikeDeckHomePage;
import com.speareducation.pages.StrikeDeckLoginPage;
import org.junit.Assert;
import org.junit.Test;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class TC_CustomerSummaryTabWebsiteLink_007 extends BaseTest {

    @Test
    public void CustomerSummaryTabWebsiteLink()throws Exception {
        System.out.println("Customer Summary Tab click on WebsiteLink Test begins...");
        StrikeDeckLoginPage sdlp = new StrikeDeckLoginPage(driver);
        sdlp.loginToSD(username,password);
        String winHandleBefore = driver.getWindowHandle();
        Thread.sleep(3000);

        Set<String> winProps = driver.getWindowHandles();
        List<String> childWindowProps = new ArrayList(winProps);
        driver.switchTo().window(childWindowProps.get(1));
        driver.navigate().to("https://spear-uat.strikedeck.com/");

        StrikeDeckHomePage homePage = new StrikeDeckHomePage(driver);
        homePage.clickCustomer360TasksTab();
        waitforElementVisible(1000);
        homePage.clickspearOnlineTab();
        waitforElementVisible(1000);

        homePage.getSearchexistingcustomer();
        Assert.assertTrue(driver.getTitle().contains("Strikedeck |"));

        CustomerDetailsPage summaryTab = new CustomerDetailsPage(driver);
        summaryTab.clicksummaryTab();
        waitforElementVisible(4000);

        CustomerSummaryPage summaryPage = new CustomerSummaryPage(driver);
        waitforElementVisible(4000);
        String generalInformationHeaderText = summaryPage.getgeneralInformationHeaderText();
        System.out.println("Header Text 1:"+generalInformationHeaderText);
        waitforElementVisible(4000);

        summaryPage.clickwebsiteLink();
        System.out.println("Website Link Has been clicked");
        Thread.sleep(5000);
        //store parent window value in string
        String parentWindow = driver.getWindowHandle();

        //store the set of all windows
        Set<String> allwindows= driver.getWindowHandles();
        int totalwindows = allwindows.size();

        String expectedLogoName = "Woodbury Dental Arts";
        int counter=0;
        for (String childWindow : allwindows) {
            if(counter<totalwindows){
                driver.switchTo().window(childWindow);
                System.out.println(driver.getTitle());
                if(driver.getTitle().equalsIgnoreCase("Dentist - Woodbury, MN - Dr. Marko Kamel")){
                    System.out.println("Web Site Title Matched");
                }
            }
            counter++;

        }
         //driver.switchTo().window(parentWindow);
        driver.quit();

    }

    }

