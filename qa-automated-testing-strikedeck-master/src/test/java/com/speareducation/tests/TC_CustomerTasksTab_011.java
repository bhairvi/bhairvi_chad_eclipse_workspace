package com.speareducation.tests;

import com.speareducation.commonLibraries.BaseTest;
import com.speareducation.pages.CustomerDetailsPage;
import com.speareducation.pages.CustomerTasksPage;
import com.speareducation.pages.StrikeDeckHomePage;
import com.speareducation.pages.StrikeDeckLoginPage;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class TC_CustomerTasksTab_011 extends BaseTest {
    @Test
    public void CustomerTasksTab() throws Exception {
        System.out.println("Customer Tasks Tab deatails Validation Test begins...");
        StrikeDeckLoginPage sdlp = new StrikeDeckLoginPage(driver);
        sdlp.loginToSD(username,password);
        String winHandleBefore = driver.getWindowHandle();
        Thread.sleep(3000);

        Set<String> winProps = driver.getWindowHandles();
        List<String> childWindowProps = new ArrayList(winProps);
        driver.switchTo().window(childWindowProps.get(1));
        driver.navigate().to("https://spear-uat.strikedeck.com/");

        StrikeDeckHomePage homePage = new StrikeDeckHomePage(driver);
        homePage.clickCustomer360TasksTab();
        waitforElementVisible(1000);
        homePage.clickspearOnlineTab();
        waitforElementVisible(1000);
        homePage.getSearchexistingcustomer();
        Assert.assertTrue(driver.getTitle().contains("Strikedeck |")  );


        //Actual test begins
        CustomerDetailsPage cdPage = new CustomerDetailsPage(driver);
        cdPage.clicktasksTab();
        System.out.println("Clicked on Tasks Tab");

        //Getting Customer Tasks Pod Header Text
        CustomerTasksPage tasksPage = new CustomerTasksPage(driver);
        String customerTasksHeaderText = tasksPage.getcustomerTasksHeaderText();
        waitforElementVisible(3000);
        System.out.println("Pod Header is :" +customerTasksHeaderText );

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,500)", "");

        //Getting Customer Tasks Total count
        waitforElementVisible(3000);
        String customerTasksTotalcount = tasksPage.getcustomerTasksTotalcount();
        System.out.println("Customer Tasks Total Records: " + customerTasksTotalcount);
        Assert.assertNotNull(customerTasksTotalcount);

        System.out.println("Customer Tasks Tab  Status Filter selection Completed Test begins...");
        //Explicit Wait set up
        WebDriverWait elmentwait = new WebDriverWait(driver, 30);
        //Hovering over "status dropdown"
        Actions action = new Actions(driver);
        Thread.sleep(2000);
        WebElement dropdown = driver.findElement(By.xpath("//*[@id=\"pod_178_5ecedb9398b22c475abb3d0a\"]/div[2]/div[2]/div/span[2]/div/button"));
        action.moveToElement(dropdown).build().perform();
        //Clicking on "status dropdown"
        Thread.sleep(2000);
        action.moveToElement(dropdown).click().perform();
        System.out.println("Applying Status Filter as completed ");

        //Selecting the checkbox containing text as "Completed"
        Thread.sleep(2000);
        CustomerTasksPage selectComplete = new CustomerTasksPage(driver);
        selectComplete.clickcompleted();
        waitforElementVisible(3000);

        CustomerTasksPage selectSearch = new CustomerTasksPage(driver);
        selectComplete.clicksearch();
        waitforElementVisible(3000);

        String rowCountContact = tasksPage.getcustomerTasksTotalcount();
        System.out.println("Customer Tasks Status Completed Total Records:" + rowCountContact);
        waitforElementVisible(5000);
        Assert.assertNotNull(rowCountContact);


       // driver.quit();

    }
}
