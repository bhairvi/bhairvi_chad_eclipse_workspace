package com.speareducation.qa;

import com.speareducation.qa.models.Browser;
import com.speareducation.qa.models.ExecutionLocation;
import org.junit.ClassRule;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class WebDriverResourceTest {
    @ClassRule
    public static final WebDriverResource res = new WebDriverResource("Unit Test Project", null);

    @Test
    public void testRuleSetup() {
        // Overrides
        assertEquals("Unit Test Project", res.getProjectName());
        assertEquals("Test Rule Setup - com.speareducation.qa.WebDriverResourceTest", res.getReportName());

        // Build Configuration
        assertEquals(Browser.CHROME, BuildConfig.browser);
        assertEquals(ExecutionLocation.LOCAL, BuildConfig.executionLocation);

        assertNotNull(res.driver);
    }

    @Test
    public void testCompositionSetup() {
        WebDriverResource webDriverResource = new WebDriverResource("Unit Test Project", "Composition Test Setup!");
        webDriverResource.setup();

        // Overrides
        assertEquals("Unit Test Project", webDriverResource.getProjectName());
        assertEquals("Composition Test Setup!", webDriverResource.getReportName());

        // Build Configuration
        assertEquals(Browser.CHROME, BuildConfig.browser);
        assertEquals(ExecutionLocation.LOCAL, BuildConfig.executionLocation);

        assertNotNull(webDriverResource.driver);

        webDriverResource.shutdown();
    }
}
