package com.speareducation.qa;

import com.speareducation.qa.models.Browser;
import com.speareducation.qa.models.ExecutionLocation;

import java.io.*;
import java.util.Properties;

public class BuildConfig {
    public static Browser browser;
    public static ExecutionLocation executionLocation;

    final static String browserConfigProperty = "QA-AUTOMATED-BROWSER";
    final static String locationConfigProperty = "QA-AUTOMATED-EXECUTION-LOCATION";
    final static String automationFile = "automation.properties";

    static {
        File configFile = new File(automationFile);
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(configFile);
        } catch (FileNotFoundException e) {
           System.out.println("File Not found " + BuildConfig.automationFile + ": " + e);
        }
        Properties props = new Properties();

        try {
            props.load(inputStream);
        } catch (IOException e) {
            System.out.println("IOException: " + e);
        }

        String browserString = props.getProperty(browserConfigProperty, Browser.CHROME.toString());
        String locationString = props.getProperty(locationConfigProperty, ExecutionLocation.LOCAL.toString());

        browser = Browser.get(browserString);
        executionLocation = ExecutionLocation.get(locationString);
    }
}
