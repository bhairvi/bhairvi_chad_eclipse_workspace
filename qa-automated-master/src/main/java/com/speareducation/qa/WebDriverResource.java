package com.speareducation.qa;

import com.speareducation.qa.models.Browser;
import com.speareducation.qa.models.ExecutionLocation;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.lang3.StringUtils;
import org.junit.rules.ExternalResource;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class WebDriverResource extends ExternalResource {
    public static final String USERNAME = System.getenv("BROWSER_STACK_USERNAME");
    public static final String AUTOMATE_KEY = System.getenv("BROWSER_STACK_AUTOMATE_KEY");
    public static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";

    /**
     * The already configured web driver for the given location and browser.
     */
    public WebDriver driver;

    final private String projectName;
    private String reportName;

    public String getProjectName() {
        return this.projectName;
    }

    public String getReportName() {
        return this.reportName;
    }

    /**
     * Setup the web driver resource. If you are not using this as a JUnit @Rule be sure to call setup and shutdown.
     * @param projectName The name of the project, this is a required field.
     * @param reportName If you are using this class as a @Rule this will auto populate, if not this is a requrired field.
     */
    public WebDriverResource(String projectName, String reportName) {
        this.projectName = projectName;
        this.reportName = reportName;
    }

    /**
     * Call this if you are not using this as a @Rule. It will configure the web driver.
     * @throws MalformedURLException
     */
    public void setup() {
        this.before();
    }

    /**
     * Call this if you not using this as a @Rule. It will clean up the web driver.
     */
    public void shutdown() {
        this.after();
    }

    @Override
    public Statement apply(Statement base, Description description) {
        this.reportName = "Unknown";
        if (description.getChildren() != null) {
            if (description.getChildren().get(0) != null) {
                Description childDescription = description.getChildren().get(0);
                String classString = childDescription.getClassName();
                String testCamelCaseString = description.getChildren().get(0).getMethodName();
                String testSentenceCaseString = StringUtils.capitalize(StringUtils.join(StringUtils.splitByCharacterTypeCamelCase(testCamelCaseString), StringUtils.SPACE));
                this.reportName = testSentenceCaseString + " - " + classString;
            }
        }

        return super.apply(base, description);
    }

    @Override
    protected void before() {
        if (BuildConfig.executionLocation == ExecutionLocation.LOCAL) {
            switch (BuildConfig.browser) {
                case CHROME:
                    WebDriverManager.chromedriver().setup();
                    driver = new ChromeDriver();
                    break;
                case IE:
                    WebDriverManager.iedriver().setup();
                    driver = new InternetExplorerDriver();
                    break;
                case FIREFOX:
                    WebDriverManager.firefoxdriver().setup();
                    driver = new FirefoxDriver();
                    break;
                case OPERA:
                    WebDriverManager.operadriver().setup();
                    driver = new OperaDriver();
                    break;
                case EDGE:
                    WebDriverManager.edgedriver().setup();
                    driver = new EdgeDriver();
                    break;
            }

            Browser browser = BuildConfig.browser;
            System.out.println("Web Driver Configured for Local Testing: ");
            System.out.println("Browser: " + browser.get());
        } else {
            LocalDateTime datetime = LocalDateTime.now();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy - HH");
            String date = datetime.format(formatter);

            Browser browser = BuildConfig.browser;

            DesiredCapabilities caps = new DesiredCapabilities();
            caps.setCapability("browser", browser.get());
            caps.setCapability("os", "Windows");
            caps.setCapability("os_version", "10");
            caps.setCapability("resolution", "1920x1080");
            caps.setCapability("project", this.projectName);
            caps.setCapability("build", date);
            caps.setCapability("name", this.reportName);

            try {
                driver = new RemoteWebDriver(new URL(URL), caps);
            } catch (Exception e) {
                System.out.println(e);
            }

            System.out.println("Web Driver Configured for Remote Testing: ");
            System.out.println("Browser: " + browser.get());
            System.out.println("OS: Windows");
            System.out.println("OS Version: 10");
            System.out.println("Resolution: 1920x1080");
            System.out.println("Project: " + this.projectName);
            System.out.println("Build: " + date);
            System.out.println("Name: " + this.reportName);
        }

        driver.manage().window().maximize();
    }

    @Override
    protected void after() {
        if (driver != null) {
            driver.quit();
        }
    }
}