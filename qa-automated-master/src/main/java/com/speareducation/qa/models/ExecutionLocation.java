package com.speareducation.qa.models;

import java.util.HashMap;
import java.util.Map;

/**
 * Possible locations that tests can be executed.
 */
public enum ExecutionLocation {
    /**
     * Execute using a remote driver.
     */
    REMOTE("REMOTE"),

    /**
     * Execute locally on this machine.
     */
    LOCAL("LOCAL");

    //Lookup table
    private static final Map<String, ExecutionLocation> lookup = new HashMap<>();

    //Populate the lookup table on loading time
    static {
        for (ExecutionLocation env : ExecutionLocation.values()) {
            lookup.put(env.get(), env);
        }
    }

    private String executionLocation;

    //****** Reverse Lookup Implementation************//

    /**
     * Retrieve the string version of the execution location.
     *
     * @return The string version of the execution location.
     */
    ExecutionLocation(String executionLocation) {
        this.executionLocation = executionLocation;
    }

    /**
     * Retrieve the typed version of the execution location string.
     *
     * @param executionLocation The string version of a execution location.
     * @return The typed version of the execution location.
     */
    public static ExecutionLocation get(String executionLocation) {
        return lookup.get(executionLocation);
    }

    //This method can be used for reverse lookup purpose

    public String get() {
        return executionLocation;
    }
}