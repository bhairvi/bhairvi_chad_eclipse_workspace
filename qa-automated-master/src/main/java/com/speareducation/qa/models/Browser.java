package com.speareducation.qa.models;

import java.util.HashMap;
import java.util.Map;

/**
 * Possible Browses testing can be performed on.
 */
public enum Browser {
    /**
     * Google Chrome
     */
    CHROME("CHROME"),

    /**
     * Microsoft Internet Explorer
     */
    IE("IE"),

    /**
     * Mozilla Firefox
     */
    FIREFOX("FIREFOX"),

    /**
     * Opera Web Browser
     */
    OPERA("OPERA"),

    /**
     * Microsoft Edge
     */
    EDGE("EDGE");

    //Lookup table
    private static final Map<String, Browser> lookup = new HashMap<>();

    //Populate the lookup table on loading time
    static {
        for (Browser env : Browser.values()) {
            lookup.put(env.get(), env);
        }
    }

    private String browser;

    //****** Reverse Lookup Implementation************//

    Browser(String browser) {
        this.browser = browser;
    }

    /**
     * Retrieve the typed version of the browser string.
     *
     * @param browser The string version of a browser.
     * @return The typed version of the browser.
     */
    public static Browser get(String browser) {
        return lookup.get(browser);
    }

    //This method can be used for reverse lookup purpose

    /**
     * Retrieve the string version of the browser.
     *
     * @return The string version of the browser.
     */
    public String get() {
        return browser;
    }
}